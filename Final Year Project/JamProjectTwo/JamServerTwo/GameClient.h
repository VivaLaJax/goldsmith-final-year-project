#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <deque>
#include <vector>
#include "GameMessage.h"
#include "ElementGraph.h"
#include "Postbox.h"

using boost::asio::ip::tcp;
using namespace std;
typedef std::deque<GameMessage> GameMessage_queue;

class GameClient : public boost::enable_shared_from_this<GameClient>, public Postbox
{
public:
  GameClient(boost::asio::io_service& io_service,
      tcp::resolver::iterator endpoint_iterator, ElementGraph& emptyGraph)
    : io_service_(io_service),
      socket_(io_service), graph(emptyGraph)
  {
    tcp::endpoint endpoint = *endpoint_iterator;
    socket_.async_connect(endpoint,
        boost::bind(&GameClient::handle_connect, this,
          boost::asio::placeholders::error, ++endpoint_iterator));
  }

  boost::asio::ip::tcp::socket& socket()
  {
    return socket_;
  }

  /// Asynchronously read a data structure from the socket.
  template <typename T, typename Handler>
  void graph_read(T& t, Handler handler)
  {
	// Issue a read operation to read exactly the number of bytes in a header.
	void (GameClient::*f)(const boost::system::error_code&, T&, boost::tuple<Handler>)
		= &GameClient::handle_graph_read_header<T, Handler>;
    boost::asio::async_read(socket_, boost::asio::buffer(inbound_header_),
        boost::bind(f,shared_from_this(), boost::asio::placeholders::error, boost::ref(t), boost::make_tuple(handler)));
  }

  /// Handle completion of a read operation.
	void GameClient::handle_graph_read(const boost::system::error_code& e)
	{
		//cout << "Graph has been read." << endl;
		
		//cout << graph.getNodes()[0]->getLocation().getName() << endl;

		boost::asio::async_read(socket_,
        boost::asio::buffer(read_msg_.data(), GameMessage::header_length),
        boost::bind(&GameClient::handle_read_header, shared_from_this(),
          boost::asio::placeholders::error));
	}

	/// Handle a completed read of a message header. The handler is passed using
  /// a tuple since boost::bind seems to have trouble binding a function object
  /// created using boost::bind as a parameter.
  template <typename T, typename Handler>
  void handle_graph_read_header(const boost::system::error_code& e, T& t, boost::tuple<Handler> handler)
  {
	if (e)
    {
		boost::get<0>(handler)(e);
    }
    else
    {
		// Determine the length of the serialized data.
		std::istringstream is(std::string(inbound_header_, header_length));
		std::size_t inbound_data_size = 0;
		if (!(is >> std::hex >> inbound_data_size))
		{
			// Header doesn't seem to be valid. Inform the caller.
			boost::system::error_code error(boost::asio::error::invalid_argument);
			boost::get<0>(handler)(error);
			return;
		}

		// Start an asynchronous call to receive the data.
		inbound_data_.resize(inbound_data_size);
		void (GameClient::*f)(const boost::system::error_code&, T&, boost::tuple<Handler>)
			= &GameClient::handle_graph_read_data<T, Handler>;
		boost::asio::async_read(socket_, boost::asio::buffer(inbound_data_),
			boost::bind(f, this, boost::asio::placeholders::error, boost::ref(t), handler));
	}
  }

  /// Handle a completed read of message data.
  template <typename T, typename Handler>
  void handle_graph_read_data(const boost::system::error_code& e, T& t, boost::tuple<Handler> handler)
  {
    if (e)
    {
		boost::get<0>(handler)(e);
    }
    else
    {
		// Extract the data structure from the data just received.
	    try
		{
			std::string archive_data(&inbound_data_[0], inbound_data_.size());
			std::istringstream archive_stream(archive_data);
			boost::archive::text_iarchive archive(archive_stream);
			archive >> t;
			//graph = t;
			
			//graphReturned = true;
			//cout << "Graph Read." << endl;

			boost::asio::async_read(socket_,
				boost::asio::buffer(read_msg_.data(), GameMessage::header_length),
				boost::bind(&GameClient::handle_read_header, this,
				boost::asio::placeholders::error));
		}
		catch (std::exception err)
		{
			// Unable to decode data.
			boost::system::error_code error(boost::asio::error::invalid_argument);
			boost::get<0>(handler)(error);
			return;
		}

		// Inform caller that data has been received ok.
		boost::get<0>(handler)(e);
	}
  }

  boost::shared_ptr<GameClient> getSharedPtr()
  {
	  return shared_from_this();
  }

  void write(const GameMessage& msg)
  {
    io_service_.post(boost::bind(&GameClient::do_write, this, msg));
  }

  void sendMessage(string message)
  {
	  GameMessage msg;
	  msg.body_length(strlen(message.c_str()));
	  memcpy(msg.body(), message.c_str(), msg.body_length());
      msg.encode_header();
      write(msg);

	  //cout << "Message written as: " + message << endl;
  }

  void close()
  {
    io_service_.post(boost::bind(&GameClient::do_close, this));
  }

private:

  void handle_connect(const boost::system::error_code& error,
      tcp::resolver::iterator endpoint_iterator)
  {
    if (!error)
    {
      graph_read(graph,boost::bind(&GameClient::handle_graph_read, this,boost::asio::placeholders::error));
    }
    else if (endpoint_iterator != tcp::resolver::iterator())
    {
      socket_.close();
      tcp::endpoint endpoint = *endpoint_iterator;
      socket_.async_connect(endpoint,
          boost::bind(&GameClient::handle_connect, this,
            boost::asio::placeholders::error, ++endpoint_iterator));
    }
  }

  void handle_read_header(const boost::system::error_code& error)
  {
    if (!error && read_msg_.decode_header())
    {
      boost::asio::async_read(socket_,
          boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
          boost::bind(&GameClient::handle_read_body, this,
            boost::asio::placeholders::error));
    }
    else
    {
      do_close();
    }
  }

  void handle_read_body(const boost::system::error_code& error)
  {
    if (!error)
    {
      //std::cout.write(read_msg_.body(), read_msg_.body_length());
      //std::cout << "\n";
	  std::string msg(read_msg_.body(), read_msg_.body_length());
	  parseMsg(msg);
      boost::asio::async_read(socket_,
          boost::asio::buffer(read_msg_.data(), GameMessage::header_length),
          boost::bind(&GameClient::handle_read_header, this,
            boost::asio::placeholders::error));
    }
    else
    {
      do_close();
    }
  }

  void do_write(GameMessage msg)
  {
    bool write_in_progress = !write_msgs_.empty();
    write_msgs_.push_back(msg);
    if (!write_in_progress)
    {
      boost::asio::async_write(socket_,
          boost::asio::buffer(write_msgs_.front().data(),
            write_msgs_.front().length()),
          boost::bind(&GameClient::handle_write, this,
            boost::asio::placeholders::error));
    }
  }

  void handle_write(const boost::system::error_code& error)
  {
    if (!error)
    {
      write_msgs_.pop_front();
      if (!write_msgs_.empty())
      {
        boost::asio::async_write(socket_,
            boost::asio::buffer(write_msgs_.front().data(),
              write_msgs_.front().length()),
            boost::bind(&GameClient::handle_write, this,
              boost::asio::placeholders::error));
      }
    }
    else
    {
      do_close();
    }
  }

  void do_close()
  {
    socket_.close();
  }

  void parseMsg(string msg)
  {
	//cout << "Message Parsed as: " + msg << endl;
	if(msg[0]=='&')
	{
		vector<string> final;
		final.push_back("");
		int currWord=0;
		for(size_t i=0; i<msg.length(); i++)
		{
			if(msg[i]==':')
			{
				currWord++;
				final.push_back("");
			}
			else
			{
				if(msg[i]!='&')
				{
					final[currWord].push_back(msg[i]);
				}
			}
		}

		if(final[0].compare("itemRem")==0)
		{
			cout << "Item Removed" << endl;
		}
	  }
	  else
	  {
		  cout << msg << endl;
	  }
  }

private:
  boost::asio::io_service& io_service_;
  tcp::socket socket_;
  GameMessage read_msg_;
  GameMessage_queue write_msgs_;

  /// The size of a fixed length header.
  enum { header_length = 8 };
  /// Holds an outbound header.
  std::string outbound_header_;
  /// Holds the outbound data.
  std::string outbound_data_;
  /// Holds an inbound header.
  char inbound_header_[header_length];
  /// Holds the inbound data.
  std::vector<char> inbound_data_;

  ElementGraph& graph;
};

typedef boost::shared_ptr<GameClient> game_client_ptr;

#endif