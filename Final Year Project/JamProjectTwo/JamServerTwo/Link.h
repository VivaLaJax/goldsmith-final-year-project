/*	Link.h
/
/	Edmund Lewry
/
/	The header file for the Link object, a child of the GameObject class.
*/

#ifndef LINK_H
#define LINK_H

#include <string>
#include <vector>
#include "GameObject.h"
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>

using namespace std;

class Link : public GameObject
{
	public:
		Link();

		//getters
		string getToLoc();
		string getFromLoc();
		string getStatDesc();
		string getDynDesc();
		vector<string> getRequiredItem();

		//setters
		void setToLoc(string loc);
		void setFromLoc(string loc);
		void setStatDesc(string desc);
		void setDynDesc(string desc);
		void setRequiredItem(vector<string> objReq);

		//search
		bool findItem(string obj);
		
	private:
		//serializes the member variables of the instance, and calls serialization of parent
		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<GameObject>(*this);
			ar & toLoc;
			ar & fromLoc;
			ar & statDesc;
			ar & dynDesc;
			ar & requiredItem;
		}
		string toLoc;
		string fromLoc;
		string statDesc;
		string dynDesc;
		vector<string> requiredItem;
};

#endif