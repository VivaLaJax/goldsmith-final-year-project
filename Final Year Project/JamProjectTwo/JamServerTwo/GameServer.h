/* GameServer.h
/	
/	Taken from Christopher M. Kohloff's chat program example. 
/	All code (with exception of inclusion of ElementGraph and closeRoom function):
/	Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
*/

#include <boost/bind.hpp>
#include <boost/asio.hpp>

#include <cstdlib>
#include <iostream>
#include <list>
#include <set>

#include "GameSession.h"
#include "GameRoom.h"
#include "ElementGraph.h"

using boost::asio::ip::tcp;

class GameServer
{
public:
	//constructor
	GameServer(boost::asio::io_service& io_service, const tcp::endpoint& endpoint, ElementGraph graph)
    : io_service_(io_service), acceptor_(io_service, endpoint), room_(graph)
	{
		//creates a game session and starts an accept for the socket for that session
		//calling handle_accept on return
		GameSession_ptr new_session(new GameSession(io_service_, room_));
		acceptor_.async_accept(new_session->socket(),
			boost::bind(&GameServer::handle_accept, this, new_session,
			boost::asio::placeholders::error));

		cout << "Server open." << endl;
	}

	//async_accept handler function
	void handle_accept(GameSession_ptr session, const boost::system::error_code& error)
	{
		//if there was no error
		if (!error)
		{
			//start the accepted session and start accept on a new session
			session->start();
			GameSession_ptr new_session(new GameSession(io_service_, room_));
			acceptor_.async_accept(new_session->socket(),
				boost::bind(&GameServer::handle_accept, this, new_session,
				boost::asio::placeholders::error));

			cout << "Connection Accepted." << endl;
		}
		else
		{
			cout << error.message() << endl;
		}
	}

	//clears all players from the room
	void closeRoom()
	{
		room_.ejectAll();
	}

private:
	boost::asio::io_service& io_service_;
	tcp::acceptor acceptor_;
	GameRoom room_;
};

typedef boost::shared_ptr<GameServer> GameServer_ptr;
typedef std::list<GameServer_ptr> GameServer_list;