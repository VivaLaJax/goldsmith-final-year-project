/* GameMessage.h
/	
/	Taken from Christopher M. Kohloff's chat program example. 
/	All code:
/	Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
*/

#ifndef GAME_MESSAGE_HPP
#define GAME_MESSAGE_HPP

#include <cstdio>
#include <cstdlib>
#include <cstring>

class GameMessage
{
public:
	enum { header_length = 4 };
	enum { max_body_length = 512 };

	//constructor
	GameMessage()
	  : body_length_(0) {}

	//getters
	const char* data() const
	{
		return data_;
	}

	char* data()
	{
		return data_;
	}

	size_t length() const
	{
		return header_length + body_length_;
	}

	const char* body() const
	{
		return data_ + header_length;
	}	

	char* body()
	{
		return data_ + header_length;
	}

	size_t body_length() const
	{
		return body_length_;
	}	

	//setters
	void body_length(size_t length)
	{
		body_length_ = length;
		if (body_length_ > max_body_length)
		  body_length_ = max_body_length;
	}

	//sets the body length based on the header contents
	//returns true if decode was successful, otherwise returns false
	bool decode_header()
	{
		using namespace std; // For strncat and atoi.
		char header[header_length + 1] = "";
		strncat(header, data_, header_length);
		body_length_ = atoi(header);
		if (body_length_ > max_body_length)
		{
			body_length_ = 0;
			return false;
		}
		return true;
	}

	//creates a header based on length of data
	void encode_header()
	{
		using namespace std; // For sprintf and memcpy.
		char header[header_length + 1] = "";
		sprintf(header, "%4d", body_length_);
		memcpy(data_, header, header_length);
	}

private:
	char data_[header_length + max_body_length];
	size_t body_length_;
};

#endif