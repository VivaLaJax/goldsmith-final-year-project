/* Enemy.cpp
//
// All code written by Edmund Lewry
//
// A class that represents a type of enemy in the game used as an opponent in combat.
*/

#include "Enemy.h"

//constructor
Enemy::Enemy()
{
	type = 3;
	strength = 0;
	speed = 0;
	endurance = 0;
	intelligence = 0;
	healthPoints = 0;
	specialPoints = 0;
	maxHealthPoints = 0;
	maxSpecialPoints = 0;
	expPoints = 0;
	enName = "";
	enDesc = "";
	vector<string> empty;
	tags = empty;
}
	
//getters
vector<int> Enemy::getAllStats()
{
	vector<int> allStats;
	allStats.push_back(strength);
	allStats.push_back(speed);
	allStats.push_back(endurance);
	allStats.push_back(intelligence);
	allStats.push_back(healthPoints);
	allStats.push_back(maxHealthPoints);
	allStats.push_back(specialPoints);
	allStats.push_back(maxSpecialPoints);
	allStats.push_back(expPoints);

	return allStats;
}

//setters
void Enemy::calculateMaxHP()
{
	maxHealthPoints = endurance*2;
}
void Enemy::calculateMaxSP()
{
	maxSpecialPoints = intelligence*2;
}

//search function - returns true if the given tag is in the collection
bool Enemy::findTag(string tag)
{
	bool retVal = false;
	
	for(int i=0; i<tags.size(); i++)
	{
		if(tag.compare(tags[i])==0)
		{
			retVal = true;
		}
	}

	return retVal;
}

//take a combat turn
string Enemy::takeTurn(PlayerCharacter& player)
{
	string retVal;

	//for now just attack the player
	int hitRoll = rand()%20;
	if((hitRoll+getStrength())>(2*player.getEndurance()))
	{
		int damageRoll = (rand()%getStrength()) + getStrength();
		if(player.getLastCommand()==1)
		{
			damageRoll-=player.getEndurance()/2;
			if(damageRoll<0)
			{
				damageRoll=0;
			}
		}
		player.loseHP(damageRoll);
		retVal = "attacked you, doing " + damageRoll;
		retVal.append(" damage.");
	}
	else
	{
		retVal = "missed you!";
	}

	return retVal;
}