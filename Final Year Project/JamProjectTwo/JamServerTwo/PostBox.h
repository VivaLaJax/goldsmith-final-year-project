/*	PostBox.h
//
//	Edmund Lewry
//
//	Written to avoid cyclic dependency in the client program architecture.
*/


#pragma once

#ifndef POSTBOX_H
#define POSTBOX_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

using namespace std;

class Postbox
{
public:
	virtual ~Postbox() {}
	virtual void sendMessage(string message)=0;
};

typedef boost::shared_ptr<Postbox> postbox_ptr;
#endif
