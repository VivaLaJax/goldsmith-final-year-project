/*	GameElement.cpp
/
/	Edmund Lewry
/
/	
/	This is the parent class for all game elements used by the Game Controller class to present
/	gameplay to the player. A gameplay element is something like a travel section or a battle,
/	which the user must complete in order to move through the map.
*/

#include "GameElement.h"
#include <iostream>

//constructor
GameElement::GameElement() {}

//getters
Location GameElement::getLocation()
{
	return location;
}

vector<KeyItem> GameElement::getKItems()
{
	return kItems;
}

vector<Enemy> GameElement::getEnemies()
{
	return enemies;
}

vector<int> GameElement::getIdentifiers()
{
	return enBattleIdentifiers;
}

//setters
void GameElement::setLocation(Location newLoc)
{
	location = newLoc;
}

void GameElement::setKItems(vector<KeyItem> items)
{
	kItems = items;
}

void GameElement::setEnemies(vector<Enemy> enems)
{
	enemies = enems;
}

void GameElement::addKItem(KeyItem newKItem)
{
	kItems.push_back(newKItem);
}

void GameElement::addEnemy(Enemy enemy, int identifier)
{
	enemies.push_back(enemy);
	enBattleIdentifiers.push_back(identifier);
}

void GameElement::removeKItem(int i)
{
	//location.removeKItem(kItems[i].getName());
	kItems.erase(kItems.begin()+i);
}

void GameElement::removeEnemy(int i)
{
	enemies.erase(enemies.begin()+i);
	enBattleIdentifiers.erase(enBattleIdentifiers.begin()+i);
}

int GameElement::findKItem(string itemName)
{
	int retVal = -99;
	string check;

	for(size_t i=0; i<kItems.size(); i++)
	{
		for(size_t j=0; j<kItems[i].getTags().size(); j++)
		{
			check = kItems[i].getTags()[j];
			if(itemName.compare(check)==0)
			{
				retVal = i;
			}
		}
	}

	return retVal;
}

//play - the main function, called by the controller, controls output of text and user input
GameElement* GameElement::play()
{
	GameElement* returnElement = new GameElement();

	return returnElement;
}

//writes text to the screen
void GameElement::writeToScreen(string toWrite)
{
	cout << toWrite << endl;
}

//retrieves user input
string GameElement::retrieveInput()
{
	string retVal;

	getline(cin,retVal);

	return retVal;
}

//delimits an input string
vector<string> GameElement::delimitInput(string input)
{
	vector<string> words;
	words.push_back("");
	int currWord = 0; 

	for(size_t i=0; i<input.size(); i++)
	{
		if(currWord>1)
		{
			words[currWord].push_back(input[i]);
		}
		else
		{
			if(input[i]==' ')
			{
				currWord++;
				words.push_back("");
			}
			else
			{
				words[currWord].push_back(input[i]);
			}
		}
	}

	return words;
}

//parses input
int GameElement::parseInput(string input)
{
	return 99;
}