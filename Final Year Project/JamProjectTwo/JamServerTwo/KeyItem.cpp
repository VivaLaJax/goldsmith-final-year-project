/*	KeyItem.cpp
/
/	Edmund Lewry
/
/	This is the KeyItem class, a child of GameObject. This class represents items that
/	can be used to unlock routes in the graph of the game. They are considered required
/	for the game to work and cannot be expended.
*/

#include "KeyItem.h"

//constructor
KeyItem::KeyItem()
{
	vector<string> empty;
	type = 1;
	kItName = "undefined";
	kItDesc = "undefined";
	tags = empty;
}

//getters
string KeyItem::getName()
{
	return kItName;
}
string KeyItem::getDesc()
{
	return kItDesc;
}
vector<string> KeyItem::getTags()
{
	return tags;
}

//setters
void KeyItem::setName(string name)
{
	kItName = name;
}
void KeyItem::setDesc(string desc)
{
	kItDesc = desc;
}
void KeyItem::setTags(vector<string> tagList)
{
	tags = tagList;
}

//search function - returns true if the given tag is in the collection
bool KeyItem::findTag(string tag)
{
	bool retVal = false;
	
	for(int i=0; i<tags.size(); i++)
	{
		if(tag.compare(tags[i])==0)
		{
			retVal = true;
		}
	}

	return retVal;
}