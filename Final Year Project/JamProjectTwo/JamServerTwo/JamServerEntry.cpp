/* JamServerEntry.cpp
/
/	Based on main function of chat_server.cpp // Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
/
/	All code written by Christopher M. Kohloff except for inclusion of StoryAgent and JSLInterpreter
*/

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include "GameServer.h"
#include "GameObject.h"
#include "ElementGraph.h"
#include "JSLInterpreter.h"
#include "StoryAgent.h"

using boost::asio::ip::tcp;

int main(int argc, char* argv[])
{
	vector<GameObject*> programObjects;
	ElementGraph storyGraph;
	try
	{
		//if no port number supplied, exit
		if (argc < 2)
		{
			std::cerr << "Usage: JamServerTwo <port> [<port> ...]\n";
			return 1;
		}

		//get objects for graph from files
		JSLInterpreter jinterp = JSLInterpreter();
		programObjects = jinterp.interpret();

		//construct story graph from objects
		StoryAgent sa = StoryAgent();
		storyGraph = sa.createStoryGraph(programObjects);

		boost::asio::io_service io_service;

		//for each port number supplied, create an end point and a server with that endpoint
		GameServer_list servers;
		for (int i = 1; i < argc; ++i)
		{
			using namespace std; // For atoi.
			tcp::endpoint endpoint(tcp::v4(), atoi(argv[i]));
			GameServer_ptr server(new GameServer(io_service, endpoint, storyGraph));
			servers.push_back(server);
		}

		//io_service.run();
		boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));

		bool open = true;

		while(open)
		{
			string line;
			std::cin >> line;

			if(line.compare("close")==0 || line.compare("Close")==0)
			{
				open = false;
			}
		}

		//close all servers
		for(size_t i=0; i<servers.size(); i++)
		{
			servers.front()->closeRoom();
		}

		io_service.stop();
		t.join();

		return 0;
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}