/* GameSession.h
/
/	Largely based on chat_server.cpp // Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
/
/	Code by Christ M. Kohloff unless otherwise stated.
/
*/

#include "GameParticipant.h"
#include "GameRoom.h"
#include "GameMessage.h"
#include "ElementGraph.h"
#include <boost/archive/text_oarchive.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <deque>
#include <string>
#include <vector>

using boost::asio::ip::tcp;
typedef std::deque<GameMessage> GameMessage_queue;

class GameSession : public GameParticipant, public boost::enable_shared_from_this<GameSession>
{
public:
	//constructor
	GameSession(boost::asio::io_service& io_service, GameRoom& room)
    : socket_(io_service), room_(room)
	{}

	//getters
	tcp::socket& socket()
	{
		return socket_;
	}

	//written by Edmund Lewry
	std::string getName()
	{
		return name;
	}

	//written by Edmund Lewry
	std::string getLoc()
	{
		return location;
	}

	//closes the socket connection
	void close()
	{
		socket_.close();
	}

	//joins the room, delivers the graph message and starts reading
	//Modified by Edmund Lewry to include deliverGraph()
	void start()
	{
		room_.join(shared_from_this());

		deliverGraph(room_.getGraph());

	    boost::asio::async_read(socket_,
        boost::asio::buffer(read_msg_.data(), GameMessage::header_length),
			boost::bind(&GameSession::handle_read_header, shared_from_this(),
			boost::asio::placeholders::error));
		cout << "Session Reading." << endl;
	}

	//creates a GameMessage object from a string (created by Edmund Lewry with code from main function
	//of Christopher M. Kohloff's chat_client.cpp example)
	void sendMessage(string message)
	{
		GameMessage msg;
		msg.body_length(strlen(message.c_str()));
		memcpy(msg.body(), message.c_str(), msg.body_length());
		msg.encode_header();
		deliver(msg);
	}

	//writes GameMessage data to the socket
	void deliver(const GameMessage& msg)
	{
		bool write_in_progress = !write_msgs_.empty();
		write_msgs_.push_back(msg);
		if (!write_in_progress)
		{
			boost::asio::async_write(socket_,
				boost::asio::buffer(write_msgs_.front().data(),
				write_msgs_.front().length()),
				boost::bind(&GameSession::handle_write, shared_from_this(),
				boost::asio::placeholders::error));
		}
	}

	//archives a graph and sends the string to socket. Created by Edmund Lewry
	//based on code from Christopher M. Kohloff's socket serialization example
	void deliverGraph(ElementGraph graph)
	{
		std::string outbound_data_;

		std::ostringstream archive_stream;
		boost::archive::text_oarchive archive(archive_stream);
		archive << graph;
		outbound_data_ = archive_stream.str();
		sendMessage(outbound_data_);
	}

	//decodes header and reads body of message, calls handle_read_body
	void handle_read_header(const boost::system::error_code& error)
	{
		if (!error && read_msg_.decode_header())
		{
			boost::asio::async_read(socket_,
				boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
				boost::bind(&GameSession::handle_read_body, shared_from_this(),
				boost::asio::placeholders::error));
		}
		else
		{
			cout << "Header Error: " << error.message() << endl;
			room_.leave(shared_from_this());
		}
	}

	//parses the message data received and begins read cycle again
	//Modified by Edmund Lewry to include message parsing
	void handle_read_body(const boost::system::error_code& error)
	{
		using namespace std;

		if (!error)
		{
			//create a string from data
			string quickCheck(read_msg_.body(), read_msg_.body_length());
			
			//if the message has a & parse it here
			if(quickCheck[0]=='&')
			{
				parseMessage(quickCheck);
			}
			//otherwise parse it in the room
			else
			{
				room_.parseMessage(read_msg_);
			}

			//start reading again
			boost::asio::async_read(socket_,
				boost::asio::buffer(read_msg_.data(), GameMessage::header_length),
				boost::bind(&GameSession::handle_read_header, shared_from_this(),
				boost::asio::placeholders::error));
		}
		else
		{
			room_.leave(shared_from_this());
		}
	}

	//async_write handler function
	//removes message from front of queue and begins write cycle again if queue not empty
	void handle_write(const boost::system::error_code& error)
	{
		if (!error)
		{
			write_msgs_.pop_front();
			if (!write_msgs_.empty())
			{
				boost::asio::async_write(socket_,
					boost::asio::buffer(write_msgs_.front().data(),
					write_msgs_.front().length()),
					boost::bind(&GameSession::handle_write, shared_from_this(),
						boost::asio::placeholders::error));
			}
		}
		else
		{
			room_.leave(shared_from_this());
		}
	}

	//parses the message received (written by Edmund Lewry)
	void parseMessage(std::string msg)
	{
		//delimit string
		std::vector<std::string> final;
		final.push_back("");
		int currWord=0;
		for(size_t i=0; i<msg.length(); i++)
		{
			if(msg[i]==':')
			{
				currWord++;
				final.push_back("");
			}
			else
			{
				if(msg[i]!='&')
				{
					final[currWord].push_back(msg[i]);
				}
			}
		}

		//if it is a name message, store the name
		if(final[0].compare("name")==0)
		{
			name = final[1];
		}
		
		//if it is a location message, store the location
		if(final[0].compare("loc")==0)
		{
			location = final[1];
		}
	}
private:
  tcp::socket socket_;
  GameRoom& room_;
  GameMessage read_msg_;
  GameMessage_queue write_msgs_;
  std::string name;
  std::string location;
};

typedef boost::shared_ptr<GameSession> GameSession_ptr;