/* GameParticipant.h
/
/	Based on chat_server.cpp // Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
/
/	deconstructor and deliver() from chat_participant by Christ M. Kohloff
/	All other written by Edmund Lewry
/
*/

#ifndef GAME_PARTICIPANT_H
#define GAME_PARTICIPANT_H

#include <boost/shared_ptr.hpp>
#include "GameMessage.h"
#include "ElementGraph.h"
#include <string>

class GameParticipant
{
public:
	virtual ~GameParticipant() {}
	virtual void deliver(const GameMessage& msg) = 0;
	virtual std::string getName()=0;
	virtual std::string getLoc()=0;
	virtual void close()=0;
	virtual void sendMessage(std::string message) = 0;
	virtual void deliverGraph(ElementGraph graph) = 0;
};

typedef boost::shared_ptr<GameParticipant> GameParticipant_ptr;

#endif