/*	TravelElement.cpp
/
/	Edmund Lewry
/
/	This is the TravelElement class, a child of the GameElement class. A travel element as a game structure
/	that provides a description of the location that the player character is in. It then expects input that tells
/	it where to travel to.
*/

#include "TravelElement.h"

//constructor
TravelElement::TravelElement()
{
	type = 0;
}

//play writes the location to screen and then looks for input until a valid
//location is specified to move to
GameElement* TravelElement::play(PlayerCharacter& player, Graph& gameGraph)
{
	bool exit = false;
	GameElement* nextElement;
	string input;
	int link = -99;

	describeLocation(player,gameGraph);
	
	while(!exit)
	{
		//if there are enemies in the area
		if(enemies.size()>0)
		{
			//have a battle
			int outcome = battle(player);

			//if the player lost
			if(outcome==0)
			{
				cout << "You've been defeated!" << endl;
				nextElement = new GameElement();
				Location temp;
				temp.setDesc("GAME_END");
				nextElement->setLocation(temp);
				writeToScreen("Thanks for Playing!");
				exit = true;
			}
			//if the player ran away
			else if(outcome==1)
			{
				//need to find out whether there is a way back to the previous location
				int link = -99;
				vector<Link> links = gameGraph.getEdges();

				for(size_t i=0; i<links.size(); i++)
				{
					if(location.getName().compare(links[i].getFromLoc())==0)
					{
						for(size_t j=0; j<gameGraph.getNodes().size(); j++)
						{
							if(links[i].getToLoc().compare(gameGraph.getNodes()[j]->getLocation().getName())==0)
							{
								vector<string> tags = gameGraph.getNodes()[j]->getLocation().getTags();
								for(size_t k=0; k<tags.size(); k++)
								{
									if(player.getLastLocation().compare(tags[k])==0)
									{
										if(player.checkInventory(links[i].getRequiredItem()))
										{
											link = i;
										}
									}
								}
							}
						}
					}
				}

				//if there is no link to the previous location, or it is unavailable
				if(link==-99)
				{
					writeToScreen("There's no way back to " + player.getLastLocation() + "!");
					writeToScreen("You are forced to turn back and fight.");
				}
				//if there is a link back then go back to it
				else
				{
					writeToScreen("You ran back to " + player.getLastLocation() + ".");
					nextElement = gameGraph.retrieveNodeFromLink(gameGraph.getEdges()[link].getToLoc());
					writeToScreen(gameGraph.getEdges()[link].getDynDesc());
					exit = true;
				}
			}
			else if(outcome==2)
			{
				//if the player won continue on
				cout << "You defeated all the enemies!" << endl;
			}
			else
			{
				//player has quit
				nextElement = new GameElement();
				Location temp;
				temp.setDesc("GAME_END");
				nextElement->setLocation(temp);
				writeToScreen("Thanks for Playing!");
				exit = true;
			}
		}
		else
		{
			//get link based on the input of the player
			link = parseInput(retrieveInput(),player,gameGraph);

			if(link!=-99)
			{
				//player has requested a close
				if(link==-98)
				{
					nextElement = new GameElement();
					Location temp;
					temp.setDesc("GAME_END");
					nextElement->setLocation(temp);
					writeToScreen("Thanks for Playing!");
					exit = true;
				}
				else
				{
					//write the links dynamic text to screen
					nextElement = gameGraph.retrieveNodeFromLink(gameGraph.getEdges()[link].getToLoc());
					writeToScreen(gameGraph.getEdges()[link].getDynDesc());
					exit = true;
				}
			}
		}
	}
	
	writeToScreen("Press enter to continue");
	retrieveInput();

	return nextElement;
}

//online version of play (all the same but passing the connection information through
GameElement* TravelElement::onlinePlay(postbox_ptr connect,PlayerCharacter& player, Graph& gameGraph)
{
	bool exit = false;
	GameElement* nextElement;
	string input;
	int link = -99;

	describeLocation(player,gameGraph);

	while(!exit)
	{
		link = parseInput(connect, retrieveInput(),player,gameGraph);

		if(link!=-99)
		{
			//player has requested a close
			if(link==-98)
			{
				nextElement = new GameElement();
				Location temp;
				temp.setDesc("GAME_END");
				nextElement->setLocation(temp);
				writeToScreen("Thanks for Playing!");
				exit = true;
			}
			else
			{
				//write the links dynamic text to screen
				nextElement = gameGraph.retrieveNodeFromLink(gameGraph.getEdges()[link].getToLoc());
				writeToScreen(gameGraph.getEdges()[link].getDynDesc());
				player.setLastLocation(location.getName());
				exit = true;
			}
		}
	}
	
	writeToScreen("Press enter to continue");
	retrieveInput();

	return nextElement;
}

//parse input returns the value -99 unless a valid GO TO command has been received
//and a valid location specified.
int TravelElement::parseInput(string input,PlayerCharacter& player, Graph& gameGraph)
{
	int retVal = -99;

	//reduce string to words
	vector<string> words = delimitInput(input);

	if(words.size()<=3)
	{
		if(words[0].compare("")!=0)
		{
			if(words[0].compare("GO")==0 || words[0].compare("go")==0)
			{
				if(words[1].compare("TO")==0 || words[1].compare("to")==0)
				{
					//a go to command has been received, parse as such
					retVal = parseCommand(0,words[2],player,gameGraph);
				}
				else
				{
					writeToScreen("Invalid Command.");
				}
			}
			else if(words[0].compare("PICK")==0 || words[0].compare("pick")==0)
			{
				if(words[1].compare("UP")==0 || words[1].compare("up")==0)
				{
					//a pick up command has been received, parse as such
					retVal = parseCommand(1,words[2],player,gameGraph);
				}
				else
				{
					writeToScreen("Invalid Command.");
				}
			}
			else if(words[0].compare("QUIT")==0 || words[0].compare("quit")==0)
			{
				retVal = -98;
			}
			else
			{
				writeToScreen("Invalid Command.");
			}	
		}
	}
	else
	{
		writeToScreen("Too Many Words!");
	}
	return retVal;
}

//parse input returns the value -99 unless a valid GO TO command has been received
//and a valid location specified.
//Online version (same as offline but passing connection info
int TravelElement::parseInput(postbox_ptr connect, string input, PlayerCharacter& player, Graph& gameGraph)
{
	int retVal = -99;

	vector<string> words = delimitInput(input);

	if(words.size()<=3)
	{
		if(words[0].compare("")!=0)
		{
			if(words[0].compare("GO")==0 || words[0].compare("go")==0)
			{
				if(words[1].compare("TO")==0 || words[1].compare("to")==0)
				{
					retVal = parseCommand(connect, 0,words[2],player,gameGraph);
				}
				else
				{
					writeToScreen("Invalid Command.");
				}
			}
			else if(words[0].compare("PICK")==0 || words[0].compare("pick")==0)
			{
				if(words[1].compare("UP")==0 || words[1].compare("up")==0)
				{
					retVal = parseCommand(connect, 1,words[2],player,gameGraph);
				}
				else
				{
					writeToScreen("Invalid Command.");
				}
			}
			else if(words[0].compare("say")==0 || words[0].compare("SAY")==0)
			{
				for(size_t j=2; j<words.size();j++)
				{
					words[1].append(" ");
					words[1].append(words[j]);
				}
				retVal = parseCommand(connect,2,words[2],player,gameGraph);
			}
			else if(words[0].compare("quit")==0 || words[0].compare("QUIT")==0)
			{
				retVal = -98;
			}
			else
			{
				writeToScreen("Invalid Command.");
			}
		}
	}
	else
	{
		writeToScreen("Not Enough Words!");
	}
	return retVal;
}

//offline parse command
int TravelElement::parseCommand(int command, string target,PlayerCharacter& player, Graph& gameGraph)
{
	int retVal = -99;
	switch(command)
	{
	//go to command
	case 0:
		{
			vector<Link> links = gameGraph.getEdges();
			
			//find the appropriate link and check whether the character has the items required to traverse
			//it. If the link does not exist or the char does not have the appropriate items
			//the return value remains unchanged.
			for(size_t i=0; i<links.size(); i++)
			{
				if(location.getName().compare(links[i].getFromLoc())==0)
				{
					for(size_t j=0; j<gameGraph.getNodes().size(); j++)
					{
						if(links[i].getToLoc().compare(gameGraph.getNodes()[j]->getLocation().getName())==0)
						{
							vector<string> tags = gameGraph.getNodes()[j]->getLocation().getTags();
							for(size_t k=0; k<tags.size(); k++)
							{
								if(target.compare(tags[k])==0)
								{
									if(player.checkInventory(links[i].getRequiredItem()))
									{
										retVal = i;
									}
								}
							}
						}
					}
				}
			}
			
			if(retVal==-99)
			{
				writeToScreen("No such place");
			}

			break;
		}
	//pick up command
	case 1:
		{
			//find the index of the item if it exists
			int itVal = findKItem(target);

			//if the index was found
			if(itVal!=-99)
			{
				//add the item to the player inventory
				player.addKItem(kItems[itVal]);
				//write it to screen
				writeToScreen("You pick up the " + kItems[itVal].getName());
				//remove it from this element
				removeKItem(itVal);
				//remove it from the graph reference
				gameGraph.retrieveNodeFromLink(location.getName())->removeKItem(itVal);
				//redescribe location in case new links are available
				describeLocation(player,gameGraph);
			}
			//if index was not found, tell player
			else
			{
				writeToScreen("There is no " + target + " here.");
			}

			break;
		}
	}

	return retVal;
}

//online version of parse
int TravelElement::parseCommand(postbox_ptr connect, int command, string target,PlayerCharacter& player, Graph& gameGraph)
{
	int retVal = -99;
	switch(command)
	{
	//go to command
	case 0:
		{
			vector<Link> links = gameGraph.getEdges();
			
			//same as offline except, sending a message to the server if link is possible
			for(size_t i=0; i<links.size(); i++)
			{
				if(location.getName().compare(links[i].getFromLoc())==0)
				{
					for(size_t j=0; j<gameGraph.getNodes().size(); j++)
					{
						if(links[i].getToLoc().compare(gameGraph.getNodes()[j]->getLocation().getName())==0)
						{
							vector<string> tags = gameGraph.getNodes()[j]->getLocation().getTags();
							for(size_t k=0; k<tags.size(); k++)
							{
								if(target.compare(tags[k])==0)
								{
									if(player.checkInventory(links[i].getRequiredItem()))
									{
										//create a go to message
										retVal = i;
										string toSend = "1|";
										toSend.append(gameGraph.getEdges()[i].getFromLoc().c_str());
										toSend.push_back('|');
										toSend.append(gameGraph.getEdges()[i].getToLoc().c_str());
										toSend.push_back('|');
										toSend.append(player.getName());

										//send message via connection
										connect->sendMessage(toSend);
									}
								}
							}
						}
					}
				}

				if(retVal==99)
				{
					writeToScreen("No such place");
				}
			}
			break;
		}
	//pick up command
	case 1:
		{
			int itVal = findKItem(target);

			if(itVal!=-99)
			{
				player.addKItem(kItems[itVal]);
				writeToScreen("You pick up the " + kItems[itVal].getName());
				
				//create a pick up message
				string toSend = "2|";
				toSend.append(location.getName());
				toSend.push_back('|');
				toSend.append(kItems[itVal].getName());
				toSend.push_back('|');
				toSend.append(player.getName());
				
				connect->sendMessage(toSend);
				
				describeLocation(player,gameGraph);
			}
			else
			{
				writeToScreen("There is no " + target + " here.");
			}

			break;
		}
	//say command
	case 2:
		{
			string toSend = "3|";
			toSend.append(location.getName());
			toSend.push_back('|');
			toSend.append(target);
			toSend.push_back('|');
			toSend.append(player.getName());
				
			connect->sendMessage(toSend);
			writeToScreen("You say " + target);
			break;
		}
	}

	return retVal;
}

//writes the description of the location and static description for all avaialable links and items
void TravelElement::describeLocation(PlayerCharacter& player, Graph& gameGraph)
{
	writeToScreen(location.getDesc());
	//for each available link write stat desc
	for(size_t i=0; i<gameGraph.getEdges().size(); i++)
	{
		if(location.getName().compare(gameGraph.getEdges()[i].getFromLoc())==0)
		{
			if(player.checkInventory(gameGraph.getEdges()[i].getRequiredItem()))
			{
				writeToScreen(gameGraph.getEdges()[i].getStatDesc());
			}
		}
	}

	//for each item write description
	for(size_t i=0; i<kItems.size(); i++)
	{
		writeToScreen("You see " + kItems[i].getDesc());
	}

	//for each enemy write description
	for(size_t i=0; i<enemies.size(); i++)
	{
		writeToScreen("There's " + enemies[i].getDesc() + " here!");
	}
}

int TravelElement::battle(PlayerCharacter& player)
{
	int outCome = -99;
	int playerToken = 99;
	//define turn order
	bool added;
	vector<int> turnOrder;
	vector<int> initiatives;

	//for each enemy
	for(size_t i=0; i<enemies.size(); i++)
	{
		added = false;
		//calculate initiative
		int enInitiative = enemies[i].getSpeed() + (rand() % 20);

		initiatives.push_back(enInitiative);

		//for each value in the turn order
		for(size_t j=0; j<turnOrder.size(); j++)
		{
			//if this enemies initiative is higher than the initiative of the enemy indexed by
			//the value at this point in the turn order, place this enemy here instead
			if(enInitiative>initiatives[turnOrder[j]])
			{
				turnOrder.insert(turnOrder.begin()+j,i);
				added = true;
			}
		}
		//if the enemy has not been added yet, it is the slowest, add it to the end
		if(!added)
		{
			turnOrder.push_back(i);
		}
	}

	//roll the players initiative and tell them about it
	int roll = rand() % 20;
	int playInitiative = player.getSpeed() + roll;
	string iniWrite = "You rolled a " + roll;
	iniWrite.append(" for initiative. That plus your speed of ");
	iniWrite.append(player.getSpeed() + " gives you an initiative of " + playInitiative);
	iniWrite.append(".");
	writeToScreen(iniWrite);
	
	initiatives.push_back(playInitiative);

	added=false;

	//insert the player into the turn order same as above
	for(size_t j=0; j<turnOrder.size(); j++)
	{
		if(playInitiative>initiatives[turnOrder[j]])
		{
			turnOrder.insert(turnOrder.begin()+j,playerToken);
			added = true;
		}
	}
	if(!added)
	{
		turnOrder.push_back(playerToken);
	}

	int currTurn = 0;
	//enter while loop
	while(outCome==-99)
	{
		//it is the players turn
		if(turnOrder[currTurn]==playerToken)
		{
			writeToScreen("It is your turn. What would you like to do?");
			outCome=parseBattleInput(retrieveInput(),player);
			while(outCome==-97)
			{
				outCome=parseBattleInput(retrieveInput(),player);
			}
		}
		//enemy turn
		else
		{
			string report = enemies[turnOrder[currTurn]].takeTurn(player);
			string enemyMessage = enemies[turnOrder[currTurn]].getName();
			enemyMessage.append(" " + enBattleIdentifiers[turnOrder[currTurn]]);
			enemyMessage.append(" " + report);
			writeToScreen(enemyMessage);

			//player has lost
			if(player.getCurrHP()<=0)
			{
				outCome=0;
			}
		}
		currTurn = (currTurn+1)%turnOrder.size();
	}

	return outCome;
}

int TravelElement::parseBattleInput(string input, PlayerCharacter& player)
{
	int retVal = -99;
	vector<string> words = delimitInput(input);
	if(words[0].compare("")!=0)
	{
		if(words[0].compare("Attack")==0 || words[0].compare("attack")==0)
		{
			if(words.size()>2)
			{
				retVal = parseBattleCommand(0,words[1],words[2],player);
			}
			else
			{
				retVal = parseBattleCommand(0,words[1],"",player);
			}
		}
		else if(words[0].compare("Defend")==0 || words[0].compare("defend")==0)
		{
			retVal = parseBattleCommand(1,"","",player);
		}
		else if(words[0].compare("Run")==0 || words[0].compare("run")==0 || words[0].compare("Escape")==0 || words[0].compare("escape")==0)
		{
			retVal = parseBattleCommand(2,"","",player);
		}
		else if(words[0].compare("quit")==0 || words[0].compare("QUIT")==0)
		{
			retVal = -98;
		}
		else
		{
			writeToScreen("Invalid Command.");
			retVal = -97;
		}
	}

	return retVal;
}

int TravelElement::parseBattleCommand(int command, string target, string identifier, PlayerCharacter& player)
{
	int retVal = -99;

	switch(command)
	{
	//attack battle command
	case 0:
		{
			int ident;
			if(identifier.compare("")==0)
			{
				ident=1;
			}
			else
			{
				ident=atoi(identifier.c_str());
			}

			//check enemy
			int targetEnemy = -99;

			for(size_t i=0; i<enemies.size(); i++)
			{
				if(target.compare(enemies[i].getName())==0 && ident==enBattleIdentifiers[i])
				{
					targetEnemy=i;
				}
			}

			if(targetEnemy==-99)
			{
				writeToScreen("Invalid target specified. Please input a different target.");
				retVal = -97;
				break;
			}
			else
			{
				int hitRoll = rand()%20;
				if((hitRoll+player.getStrength())>(2*enemies[targetEnemy].getEndurance()))
				{
					writeToScreen("You hit with a roll of " + hitRoll);
					int damageRoll = (rand()%player.getStrength()) + player.getStrength();

					enemies[targetEnemy].setCurrHP(enemies[targetEnemy].getCurrHP()-damageRoll);
					string toWrite = "The " + enemies[targetEnemy].getName();
					toWrite.append("is hit for " + damageRoll);
					toWrite.append(" damage!");
					writeToScreen(toWrite);

					if(enemies[targetEnemy].getCurrHP()<=0)
					{
						writeToScreen("You slay the " + enemies[targetEnemy].getName());
						enemies.erase(enemies.begin()+targetEnemy);
						enBattleIdentifiers.erase(enBattleIdentifiers.begin()+targetEnemy);
					}
					
					if(enemies.size()==0)
					{
						retVal = 2;
					}
				}
				else
				{
					string toWrite = "You missed the " + enemies[targetEnemy].getName() + " with a roll of ";
					toWrite.append(""+hitRoll);
					writeToScreen(toWrite);
				}
			}
			player.setLastCommand(0);
			break;
		}
	//defend battle command
	case 1:
		{
			writeToScreen("You steel yourself for the oncoming blows. Incoming damage reduced by Endurance/2: " + player.getEndurance()/2);
			player.setLastCommand(1);
			break;
		}
	//run battle command
	case 2:
		{
			int runRoll = (rand()%20) + player.getSpeed();
			writeToScreen("You attempted to run from battle with a roll of " + runRoll);
			bool runSuccessful = true;
			for(size_t i=0; i<enemies.size(); i++)
			{
				if(runRoll<=enemies[i].getSpeed())
				{
					runSuccessful=false;
				}
			}
			
			if(runSuccessful)
			{
				writeToScreen("You manage to escape the fight.");
				retVal = 1;
			}
			else
			{
				writeToScreen("Your opponents are too fast, they manage to cut off your escape route.");
			}
			player.setLastCommand(2);
			break;
		}
	}

	return retVal;
}