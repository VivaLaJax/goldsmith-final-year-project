/*	KeyItem.h
/
/	Edmund Lewry
/
/	The header file for the KeyItem object, a child of the GameObject class.
*/

#ifndef KITEM_H
#define KITEM_H

#include <string>
#include <vector>
#include "GameObject.h"
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>

using namespace std;

class KeyItem : public GameObject
{
	public:
		KeyItem();

		//getters
		string getName();
		string getDesc();
		vector<string> getTags();

		//setters
		void setName(string name);
		void setDesc(string desc);
		void setTags(vector<string> tagList);

		//search
		bool findTag(string tag);
		
	private:
		//serializes the member variables of the instance, and calls serialization of parent
		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<GameObject>(*this);
			ar & kItName;
			ar & kItDesc;
			ar & tags;
		}
		string kItName;
		string kItDesc;
		vector<string> tags;
};

#endif