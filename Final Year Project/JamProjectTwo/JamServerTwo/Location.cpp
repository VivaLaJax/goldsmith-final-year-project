/*	Location.cpp
/
/	Edmund Lewry
/
/	This is Location object, which is a child of the GameObject class. The Location object is used by Game Elements to
/	give information about the surroundings and it contains links to other locations. They form the backbone of the assembled
/	GameElements.
*/

#include "Location.h"

//constructor
Location::Location()
{
	vector<string> empty;
	type = 0;
	locName = "undefined";
	locDesc = "undefined";
	tags = empty;
	keyItems = empty;
	enemyList = empty;
}

//getters
string Location::getName()
{
	return locName;
}
string Location::getDesc()
{
	return locDesc;
}
vector<string> Location::getTags()
{
	return tags;
}

vector<string> Location::getKeyItems()
{
	return keyItems;
}

vector<string> Location::getEnemyList()
{
	return enemyList;
}

//setters
void Location::setName(string name)
{
	locName = name;
}
void Location::setDesc(string desc)
{
	locDesc = desc;
}
void Location::setTags(vector<string> tagList)
{
	tags = tagList;
}

void Location::setKeyItems(vector<string> kItems)
{
	keyItems = kItems;
}

void Location::addKeyItem(string kItem)
{
	keyItems.push_back(kItem);
}

void Location::setEnemyList(vector<string> enemies)
{
	enemyList = enemies;
}

void Location::addEnemy(string enemy)
{
	enemyList.push_back(enemy);
}

//remove an item from the list of items
void Location::removeKItem(string kItem)
{
	for(int i=0; i<keyItems.size(); i++)
	{
		if(kItem.compare(keyItems[i])==0)
		{
			keyItems.erase(keyItems.begin()+i);
		}
	}
}

//remove an enemy from the list of enemies
void Location::removeEnemy(string enemy)
{
	for(int i=0; i<enemyList.size(); i++)
	{
		if(enemy.compare(enemyList[i])==0)
		{
			enemyList.erase(enemyList.begin()+i);
		}
	}
}

//search function - returns true if the given tag is in the collection
bool Location::findTag(string tag)
{
	bool retVal = false;
	
	for(int i=0; i<tags.size(); i++)
	{
		if(tag.compare(tags[i])==0)
		{
			retVal = true;
		}
	}

	return retVal;
}