/* GameRoom.h
//
//	Based on chat_server.cpp // Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//	Modified heavily by Edmund Lewry.
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//	Code written by Edmund Lewry unless otherwise stated.
//
*/

#ifndef	GAMEROOM_H
#define GAMEROOM_H

#include "GameParticipant.h"
#include "ElementGraph.h"
#include "GameMessage.h"
#include <boost/bind.hpp>
#include <set>
#include <deque>
#include <vector>
#include <string>

using namespace std;

using boost::asio::ip::tcp;

typedef std::deque<GameMessage> GameMessage_queue;

class GameRoom
{
public:
	//constructor
	GameRoom(ElementGraph graph)
	{
		originalGraph = graph;
		
		//create game graph (with new pointers) to edit
		for(size_t i=0; i<graph.getNodes().size(); i++)
		{
			TravelElement* temp = new TravelElement();
			temp->setLocation(graph.getNodes()[i]->getLocation());
			temp->setKItems(graph.getNodes()[i]->getKItems());
			gameGraph.addNode(temp);
		}
		gameGraph.setEdges(graph.getEdges());
	}

	//getter
	ElementGraph getGraph()
	{
		return gameGraph;
	}

	//adds participant to collection (based on Kohloff's GameRoom.join function)
	void join(GameParticipant_ptr participant)
	{
		participants_.insert(participant);

		cout << "Client Joined." << endl;
	}

	//removes participant from collection (written by Christopher M. Kohloff)
	void leave(GameParticipant_ptr participant)
	{
		participants_.erase(participant);

		if(participants_.size()==0)
		{
			resetRoom();
		}
	}

	//delivers a message to all participants (written by Christopher M. Kohloff)
	void deliver(const GameMessage& msg)
	{
		recent_msgs_.push_back(msg);
		while (recent_msgs_.size() > max_recent_msgs)
			recent_msgs_.pop_front();

		std::for_each(participants_.begin(), participants_.end(),
			boost::bind(&GameParticipant::deliver, _1, boost::ref(msg)));
	}

	//parses an incoming message
	void parseMessage(const GameMessage& msg)
	{
		vector<string> msgValues;
		int type;

		//break down the message
		msgValues = delimitMessage(msg);
		type = atoi(msgValues[0].c_str());

		switch(type)
		{
		//leaving message
		case 0:
			//stuff
			break;
		//moving room message
		case 1:
			playerMoveAlert(msgValues);
			break;
		//pick up item message
		case 2:
			playerPickupAlert(msgValues);
			break;
		}
	}

	//set the game graph to the original graph
	void resetRoom()
	{
		cout << "Room reset to beginning." << endl;
		gameGraph.setNodes(originalGraph.getNodes());
	}

	//close all participants
	void ejectAll()
	{
		for (std::set<GameParticipant_ptr>::iterator it=participants_.begin(); it!=participants_.end(); ++it)
		{
			GameParticipant_ptr ptr = *it;
			ptr->close();
			leave(ptr);
		}
	}

private:
	//breaks a message string into seperate parts
	std::vector<std::string> delimitMessage(const GameMessage& msg)
	{
		string fullString(msg.body(), msg.body_length());
		int currWord = 0;
		vector<string> tempString;
		tempString.push_back("");

		for(size_t i = 0; i<fullString.length(); i++)
		{
			if(fullString[i]=='|')
			{
				currWord++;
				tempString.push_back("");
			}
			else
			{
				tempString[currWord].push_back(fullString[i]);
			}
		}

		return tempString;
	}
	
	//when a participant has changed node
	void playerMoveAlert(vector<string> msgVals)
	{
		vector<string> collectNames;
		for (std::set<GameParticipant_ptr>::iterator it=participants_.begin(); it!=participants_.end(); ++it)
		{
			GameParticipant_ptr ptr = *it;
			
			if(ptr->getName().compare(msgVals[3])!=0)
			{
				//tell all players in prev room that player has left
				if(ptr->getLoc().compare(msgVals[1])==0)
				{
					string message = "!" + msgVals[3] + " moves on.";
					ptr->sendMessage(message);
				}
				//tell all players in new room that player has appeared
				if(ptr->getLoc().compare(msgVals[2])==0)
				{
					string message = "!" + msgVals[3] + " has arrived.";
					ptr->sendMessage(message);
					//add name to a collection of names for later
					collectNames.push_back(ptr->getName());
				}
			}
		}


		if(collectNames.size()>0)
		{
			//tell this player what all the people in new room are like
			for (std::set<GameParticipant_ptr>::iterator it=participants_.begin(); it!=participants_.end(); ++it)
			{
				GameParticipant_ptr ptr = *it;
				if(ptr->getName().compare(msgVals[3])==0)
				{
					string message;
	
					for(size_t i=0; i<collectNames.size(); i++)
					{
						message = message + collectNames[i] + " ";
					}

					message = "!" + message + "is already here.";
					ptr->sendMessage(message);
					cout << "Message sent as: " + message << endl;
				}
			}
		}
	}

	//when a participant has picked up an item
	void playerPickupAlert(vector<string> msgVals)
	{
		for (std::set<GameParticipant_ptr>::iterator it=participants_.begin(); it!=participants_.end(); ++it)
		{
			GameParticipant_ptr ptr = *it;

			//remove item from graph
			for(size_t i=0; i<gameGraph.getNodes().size(); i++)
			{
				//find the location
				if(msgVals[1].compare(gameGraph.getNodes()[i]->getLocation().getName())==0)
				{
					GameElement* node = gameGraph.getNodes()[i];
					//remove item from location and element
					for(size_t j=0; j<node->getKItems().size(); j++)
					{
						if(msgVals[2].compare(node->getKItems()[j].getName())==0)
						{
							node->removeKItem(j);
							node->getLocation().removeKItem(msgVals[2]);
						}
					}
				}				
			}

			//tell connections to remove item from graph
			string message = "!&itemRem:" + msgVals[1] + ":" + msgVals[2];
			ptr->sendMessage(message);
			
			//if connection is in loc tell them player picked up item
			if(ptr->getName().compare(msgVals[3])!=0)
			{
				message = "!" + msgVals[3] + " has taken the " + msgVals[2];
				ptr->sendMessage(message);
			}
		}
	}

  std::set<GameParticipant_ptr> participants_;
  enum { max_recent_msgs = 100 };
  GameMessage_queue recent_msgs_;
  ElementGraph gameGraph;
  ElementGraph originalGraph;
};

#endif