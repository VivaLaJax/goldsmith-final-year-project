/*	PlayerCharacter.cpp
/
/	Edmund Lewry
/
/	The PlayerCharacter class is the object that defines the player's abilities and skills within
/	the game. It keeps track of their inventory, current area, skill values and so on.
*/

#include "PlayerCharacter.h"

//constructor
PlayerCharacter::PlayerCharacter()
{
	name = "undefined";
	strength = 5;
	speed = 5;
	endurance = 5;
	intelligence = 5;
	calculateHP();
	currHP = maxHP;
	lastCommand = 0;
}

//returns true of the collection of strings is a subset of the player's inventory
bool PlayerCharacter::checkInventory(vector<string> toCheck)
{
	int count = 0;

	for(int i=0; i<toCheck.size(); i++)
	{
		for(int j=0; j<inventory.size(); j++)
		{
			if(toCheck[i].compare(inventory[j].getName())==0)
			{
				count++;
			}
		}
	}

	//if all elements in toCheck are accounted for or toCheck is empty return true
	if(count == toCheck.size())
	{
		return true;
	}
	else if(toCheck.size()==1 && toCheck[0].compare("")==0)
	{
		return true;
	}

	return false;
}

//adds an key item to the inventory unless it already exists
void PlayerCharacter::addKItem(KeyItem newKItem)
{
	bool alreadyContains = false;

	for(size_t i=0; i<inventory.size(); i++)
	{
		if(inventory[i].getName().compare(newKItem.getName())==0)
		{
			alreadyContains = true;
		}
	}
	
	if(!alreadyContains)
	{
		inventory.push_back(newKItem);
	}
}

//setters
void PlayerCharacter::setName(string set)
{
	name = set;
}

void PlayerCharacter::calculateHP()
{
	maxHP = endurance*2;
}

void PlayerCharacter::loseHP(int toLose)
{
	currHP-=toLose;
}

void PlayerCharacter::setLastCommand(int set)
{
	lastCommand = set;
}

void PlayerCharacter::setLastLocation(string location)
{
	previousLocation = location;
}
//getters
string PlayerCharacter::getName()
{
	return name;
}

int PlayerCharacter::getStrength()
{
	return strength;
}

int PlayerCharacter::getSpeed()
{
	return speed;
}

int PlayerCharacter::getEndurance()
{
	return endurance;
}

int PlayerCharacter::getIntelligence()
{
	return intelligence;
}

int PlayerCharacter::getCurrHP()
{
	return currHP;
}

int PlayerCharacter::getMaxHP()
{
	return maxHP;
}

int PlayerCharacter::getLastCommand()
{
	return lastCommand;
}

string PlayerCharacter::getLastLocation()
{
	return previousLocation;
}