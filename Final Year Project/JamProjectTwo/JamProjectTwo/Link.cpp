/*	Link.cpp
/
/	Edmund Lewry
/
/	This is the Link class, a child of GameObject. 
*/

#include "Link.h"

//constructor
Link::Link()
{
	vector<string> empty;
	type = 2;
	toLoc = "undefined";
	fromLoc = "undefined";
	statDesc = "undefined";
	dynDesc = "undefined";
	requiredItem = empty;
}

//getters
string Link::getToLoc()
{
	return toLoc;
}
string Link::getFromLoc()
{
	return fromLoc;
}
string Link::getStatDesc()
{
	return statDesc;
}
string Link::getDynDesc()
{
	return dynDesc;
}
vector<string> Link::getRequiredItem()
{
	return requiredItem;
}

//setters
void Link::setToLoc(string loc)
{
	toLoc = loc;
}
void Link::setFromLoc(string loc)
{
	fromLoc = loc;
}
void Link::setStatDesc(string desc)
{
	statDesc = desc;
}
void Link::setDynDesc(string desc)
{
	dynDesc = desc;
}
void Link::setRequiredItem(vector<string> objReq)
{
	requiredItem = objReq;
}

//search function - returns true if the given item is in the collection
bool Link::findItem(string obj)
{
	bool retVal = false;
	
	for(int i=0; i<requiredItem.size(); i++)
	{
		if(obj.compare(requiredItem[i])==0)
		{
			retVal = true;
		}
	}

	return retVal;
}