/*	GameObject.cpp
/
/	Edmund Lewry
/
/
/	The GameObject class is the parent class for all the objects that a Game Element may use. They are things
/	like NPCs, Enemies, Locations and so on.
*/

#include "GameObject.h"

//constructor
GameObject::GameObject() {}

//getters
vector<string> GameObject::getTags()
{
	vector<string> retVal;
	return retVal;
}
vector<string> GameObject::getKeyItems()
{
	vector<string> retVal;
	return retVal;
}
vector<string> GameObject::getEnemyList()
{
	vector<string> retVal;
	return retVal;
}
vector<string> GameObject::getRequiredItem()
{
	vector<string> retVal;
	return retVal;
}

vector<int> GameObject::getAllStats()
{
	vector<int> retVal;
	return retVal;
}