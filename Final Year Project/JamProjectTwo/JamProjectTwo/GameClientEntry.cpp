/* GameClientEntry.cpp
/	
/	Uses some code from the chat_client.cpp example // Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
/	This code will be stated as such. All other code written by Edmund Lewry.
/
*/

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include "GameClientEntry.h"
#include "GameClient.h"
#include "JSLInterpreter.h"
#include "StoryAgent.h"
#include "GameController.h"
#include <vector>
#include <string>

#include "ElementGraph.h"
#include "GameObject.h"
#include "TravelElement.h"

using boost::asio::ip::tcp;

//returns which game type the user has requested
int GameClientEntry::getGameMode()
{
	cout << "Would you like to play Singleplayer(1) or Multiplayer(2)?" << endl;
	cout << "1. Singleplayer \n2. Multiplayer " << endl;
	string tempLine;
	cin >> tempLine;
	int single = atoi(tempLine.c_str());

	if(single == 1)
	{
		return 0;
	}
	
	return 1;
}

//gets the address of the host for a multiplayer game server
string GameClientEntry::getHostName()
{
	cout << "Please type host ip:" << endl;
	string host;
	cin >> host;

	return host;
}

//gets the port number for a multiplayer game server
string GameClientEntry::getPort()
{
	cout << "Please type port:" << endl;
	string port;
	cin >> port;

	return port;
}

//empty handler function
void GameClientEntry::handle_read(const boost::system::error_code& e)
{

}

//Starts the game running
void GameClientEntry::start()
{
	//find out what type of game the player would like to play
	int mode = getGameMode();

	//mode is singleplayer
	if(mode==0)
	{
		//interpret program object
		JSLInterpreter jinterp = JSLInterpreter();
		programObjects = jinterp.interpret();

		//construct story graph from objects
		StoryAgent sa = StoryAgent();
		storyGraph = sa.createStoryGraph(programObjects);

		//play game
		GameController game = GameController(storyGraph);
		game.begin();
	}
	//mode is multiplayer
	if(mode==1)
	{
		//get the host ip
		string host = getHostName();
		//get the port number
		string port = getPort();

		//the following lines taken from Christopher M. Kohloff's Example 
		boost::asio::io_service io_service;

		tcp::resolver resolver(io_service);
		tcp::resolver::query query(host, port);
		tcp::resolver::iterator iterator = resolver.resolve(query);

		//modified to include storygraph
		boost::shared_ptr<GameClient> c(new GameClient (io_service, iterator, storyGraph));
		
		boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));
		//end from Example
		

		//play game
		GameController game = GameController(storyGraph);
		game.begin(c);

		//also from example
		c->close();
		t.join();
	}
}

int main(int argc, char* argv[])
{
	GameClientEntry intControl;

	intControl.start();

	return 0;
}