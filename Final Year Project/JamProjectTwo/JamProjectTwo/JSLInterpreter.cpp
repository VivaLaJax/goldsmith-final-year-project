/*	JSLInterpreter.cpp
/
/	Edmund Lewry
/
/	This is the class that deals with interpreting a JSL Program and outputs a collection of Game Objects
/	built from the program.
*/

#include "JSLInterpreter.h"

//constructor
JSLInterpreter::JSLInterpreter() {}

//interpret function called by the main class to begin analysis
vector<GameObject*> JSLInterpreter::interpret()
{	
	ifstream reader;

	//get locations
	string filename = "locationobjects.txt";
	reader.open(filename);
    
	if(reader.good())
	{
		boost::archive::text_iarchive ia(reader);

		//read vector of locations from file
		vector<Location> tempLoc;
		ia >> tempLoc;
		
		//for each location create a pointer and add it to the game objects collection
		for(int i=0; i<tempLoc.size(); i++)
		{
			Location* locPtr = new Location();
			
			locPtr->setName(tempLoc[i].getName());
			locPtr->setDesc(tempLoc[i].getDesc());
			locPtr->setKeyItems(tempLoc[i].getKeyItems());
			locPtr->setTags(tempLoc[i].getTags());
			locPtr->setEnemyList(tempLoc[i].getEnemyList());
			outObjs.push_back(locPtr);
		}
	}

	reader.close();

	//repeat process with key items
	filename = "kitemobjects.txt";
	reader.open(filename);

	if(reader.good())
	{
		boost::archive::text_iarchive ia(reader);

		vector<KeyItem> tempKIt;
		ia >> tempKIt;
		
		for(int i=0; i<tempKIt.size(); i++)
		{
			KeyItem* kItPtr = new KeyItem();
		
			kItPtr->setName(tempKIt[i].getName());
			kItPtr->setDesc(tempKIt[i].getDesc());
			kItPtr->setTags(tempKIt[i].getTags());
			outObjs.push_back(kItPtr);
		}
	}

	reader.close();

	//repeat process with links
	filename = "linkobjects.txt";
	reader.open(filename);

	if(reader.good())
	{
		boost::archive::text_iarchive ia(reader);

		vector<Link> tempLin;
		ia >> tempLin;

		for(int i=0; i<tempLin.size(); i++)
		{
			
			Link* linPtr = new Link();
			
			linPtr->setToLoc(tempLin[i].getToLoc());
			linPtr->setFromLoc(tempLin[i].getFromLoc());
			linPtr->setStatDesc(tempLin[i].getStatDesc());
			linPtr->setDynDesc(tempLin[i].getDynDesc());
			linPtr->setRequiredItem(tempLin[i].getRequiredItem());
			outObjs.push_back(linPtr);
		}
	}

	reader.close();

	//repeat process with enemies
	filename = "enemyobjects.txt";
	reader.open(filename);

	if(reader.good())
	{
		boost::archive::text_iarchive ia(reader);

		vector<Enemy> tempEnemy;
		ia >> tempEnemy;

		for(int i=0; i<tempEnemy.size(); i++)
		{
			Enemy* enemyPtr = new Enemy();
			
			enemyPtr->setName(tempEnemy[i].getName());
			enemyPtr->setDesc(tempEnemy[i].getDesc());
			enemyPtr->setTags(tempEnemy[i].getTags());
			enemyPtr->setStrength(tempEnemy[i].getStrength());
			enemyPtr->setSpeed(tempEnemy[i].getSpeed());
			enemyPtr->setEndurance(tempEnemy[i].getEndurance());
			enemyPtr->setIntelligence(tempEnemy[i].getIntelligence());
			enemyPtr->calculateMaxHP();
			enemyPtr->calculateMaxSP();
			enemyPtr->setCurrHP(enemyPtr->getMaxHP());
			enemyPtr->setCurrSP(enemyPtr->getMaxSP());
			enemyPtr->setXP(tempEnemy[i].getXPVal());
			outObjs.push_back(enemyPtr);
		}
	}

	reader.close();

	return outObjs;
}