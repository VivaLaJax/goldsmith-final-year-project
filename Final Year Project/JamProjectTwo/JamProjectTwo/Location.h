/*	Location.h
/
/	Edmund Lewry
/
/	The header file for the Location object, a child of the GameObject class.
*/

#ifndef LOCATION_H
#define LOCATION_H

#include <string>
#include <vector>
#include "GameObject.h"
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>

using namespace std;

class Location : public GameObject
{
	public:
		Location();

		//getters
		string getName();
		string getDesc();
		vector<string> getTags();
		vector<string> getKeyItems();
		vector<string> getEnemyList();

		//setters
		void setName(string name);
		void setDesc(string desc);
		void setTags(vector<string> tagList);
		void setKeyItems(vector<string> kItems);
		void addKeyItem(string kItem);
		void setEnemyList(vector<string> enemies);
		void addEnemy(string enemy);

		void removeKItem(string kItem);
		void removeEnemy(string enemy);

		//search
		bool findTag(string tag);
		
	private:
		//serializes the member variables of the instance, and calls serialization of parent
		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<GameObject>(*this);
			ar & locName;
			ar & locDesc;
			ar & tags;
			ar & keyItems;
			ar & enemyList;
		}
		string locName;
		string locDesc;
		vector<string> tags;
		vector<string> keyItems;
		vector<string> enemyList;
};

#endif