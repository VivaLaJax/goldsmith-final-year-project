/* StoryAgent.cpp
/	Edmund Lewry
/
/	The story agent class is the object that takes the set of objects that were created by the interpreter
/	and chooses a set to make into a coherent set of gameplay elements that the game controller will use to
/	present to the player.
/
*/

#include "StoryAgent.h"

//constructor
StoryAgent::StoryAgent()
{

}

//creates a graph of gameplay elements from list of all program objects
ElementGraph StoryAgent::createStoryGraph(vector<GameObject*> programObjects)
{
	ElementGraph eg = ElementGraph();
	availableObjects = programObjects;

	//for each object in the vector check the type
	for(size_t i=0; i<availableObjects.size(); i++)
	{
		cout << "Object Type: " << availableObjects[i]->getType() << endl;
		if(availableObjects[i]->getType()==0)
		{
			//its a Location object
			Location loc;
			TravelElement* element = new TravelElement();
			loc = makeLocation(i);
		
			element->setLocation(loc);
				
			eg.addNode(element);
		}

		if(availableObjects[i]->getType()==1)
		{
			//its a KeyItem
			KeyItem kIt;
			kIt = makeKItem(i);
			allKItems.push_back(kIt);
		}

		if(availableObjects[i]->getType()==2)
		{
			//its a Link
			Link lin;
			lin = makeLink(i);
			eg.addEdge(lin);
		}
		if(availableObjects[i]->getType()==3)
		{
			//its a type of Enemy
			Enemy enemy;
			enemy = makeEnemy(i);
			allEnemyTypes.push_back(enemy);
		}
	}


	//for each key item, place it in its appropriate location, or in a random
	//location if it has no set place.
	for(size_t i=0; i<allKItems.size(); i++)
	{
		if(!eg.addKItem(allKItems[i]))
		{
			//pick a random location and add it
			int ran = rand() % eg.getNodes().size();

			eg.getNodes()[ran]->addKItem(allKItems[i]);
		}
	}

	//for each enemy type, add it to the graph
	for(size_t i=0; i<allEnemyTypes.size(); i++)
	{
		eg.addEnemy(allEnemyTypes[i]);
	}

	return eg;
}

//create a location from the GameObject pointer
Location StoryAgent::makeLocation(int i)
{
	Location retLoc;
	
	if(availableObjects[i]->getType()==0)
	{
		retLoc.setName(availableObjects[i]->getName());
		retLoc.setDesc(availableObjects[i]->getDesc());
		retLoc.setTags(availableObjects[i]->getTags());
		retLoc.setKeyItems(availableObjects[i]->getKeyItems());
		retLoc.setEnemyList(availableObjects[i]->getEnemyList());
	}

	return retLoc;
}

//create a KeyItem from a GameObject pointer
KeyItem StoryAgent::makeKItem(int i)
{
	KeyItem retKItem;

	if(availableObjects[i]->getType()==1)
	{
		retKItem.setName(availableObjects[i]->getName());
		retKItem.setDesc(availableObjects[i]->getDesc());
		retKItem.setTags(availableObjects[i]->getTags());
	}

	return retKItem;
}

//create a Link from a GameObject pointer
Link StoryAgent::makeLink(int i)
{
	Link retLin;

	if(availableObjects[i]->getType()==2)
	{
		retLin.setToLoc(availableObjects[i]->getToLoc());
		retLin.setFromLoc(availableObjects[i]->getFromLoc());
		retLin.setStatDesc(availableObjects[i]->getStatDesc());
		retLin.setDynDesc(availableObjects[i]->getDynDesc());
		retLin.setRequiredItem(availableObjects[i]->getRequiredItem());
	}

	return retLin;
}

//create an Enemy from a GameObject pointer
Enemy StoryAgent::makeEnemy(int i)
{
	Enemy retEnemy;

	if(availableObjects[i]->getType()==3)
	{
		retEnemy.setName(availableObjects[i]->getName());
		retEnemy.setDesc(availableObjects[i]->getDesc());
		retEnemy.setTags(availableObjects[i]->getTags());
		retEnemy.setStrength(availableObjects[i]->getStrength());
		retEnemy.setSpeed(availableObjects[i]->getSpeed());
		retEnemy.setEndurance(availableObjects[i]->getEndurance());
		retEnemy.setIntelligence(availableObjects[i]->getIntelligence());
		retEnemy.calculateMaxHP();
		retEnemy.calculateMaxSP();
		retEnemy.setCurrHP(retEnemy.getMaxHP());
		retEnemy.setCurrSP(retEnemy.getMaxSP());
		retEnemy.setXP(availableObjects[i]->getXPVal());
	}

	return retEnemy;
}