/*	GameElement.h
/	
/	Edmund Lewry
/
/
/	The header file for the game element class.
*/

#ifndef GAMEELEMENT_H
#define GAMEELEMENT_H

#include <string>
#include "Location.h"
#include "KeyItem.h"
#include "Enemy.h"
#include "PlayerCharacter.h"
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/split_member.hpp>

using namespace std;

class GameElement
{
	public:
		GameElement();
		virtual GameElement* play();

		//getters
		virtual Location getLocation();
		virtual vector<KeyItem> getKItems();
		virtual vector<Enemy> getEnemies();
		virtual vector<int> getIdentifiers();
		int getType() { return type; }

		//setters
		void setLocation(Location location);
		void setKItems(vector<KeyItem> items);
		void setEnemies(vector<Enemy> enems);
		void addKItem(KeyItem newKItem);
		void addEnemy(Enemy enemyType, int identifier);
		void removeEnemy(int i);
		void removeKItem(int i);
		//search
		int findKItem(string itemName);

	protected:
		//serializes the member variables of the instance
		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & kItems;
			ar & location;
			ar & type;
			ar & enemies;
			ar & enBattleIdentifiers;
		}

		//io
		void writeToScreen(string toWrite);
		string retrieveInput();

		virtual int parseInput(string input);
		vector<string> delimitInput(string input);
		vector<KeyItem> kItems;
		Location location;
		vector<Enemy> enemies;
		vector<int> enBattleIdentifiers;
		int type;
};

#endif
