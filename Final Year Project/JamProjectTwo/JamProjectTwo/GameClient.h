/* GameClient.h
/
/	Based on chat_client.cpp // Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
/
/	Code written by Christopher M. Kohloff unless otherwise stated
/
*/

#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <deque>
#include <vector>
#include "GameMessage.h"
#include "ElementGraph.h"
#include "TravelElement.h"
#include "Postbox.h"

using boost::asio::ip::tcp;
using namespace std;

//queue of messages to send
typedef std::deque<GameMessage> GameMessage_queue;

//inherits from boost enable shared from this to allow shared pointer creation
//inherits from Postbox to prevent cyclic dependency
class GameClient : public boost::enable_shared_from_this<GameClient>, public Postbox
{
public:
	//constructor (modified to include graph by Edmund Lewry)
	GameClient(boost::asio::io_service& io_service, tcp::resolver::iterator iterator, ElementGraph& emptyGraph)
    : io_service_(io_service), socket_(io_service), graph(emptyGraph)
	{
		tcp::endpoint endpoint = *iterator;

		//attempt to connect to the tcp endpoint, call handle connect when operation returns
		//with pointer to next resolver as a parameter
		socket_.async_connect(endpoint, boost::bind(&GameClient::handle_connect, this,
			boost::asio::placeholders::error, ++iterator));
	}

	//getter for socket
	boost::asio::ip::tcp::socket& socket()
	{
		return socket_;
	}

	//getter for shared pointer (written by Edmund Lewry)
	boost::shared_ptr<GameClient> getSharedPtr()
	{
		return shared_from_this();
	}

	//starts the async reading cycle
	void beginReading()
	{
		//read from the socket to read_msg_ data, header length number of chars
		//call handle read header when return
		boost::asio::async_read(socket_,
			boost::asio::buffer(read_msg_.data(), GameMessage::header_length),
			boost::bind(&GameClient::handle_read_header, this,
            boost::asio::placeholders::error));
	}

	//adds a write function call to the io_service's work load
	void write(const GameMessage& msg)
	{
		io_service_.post(boost::bind(&GameClient::do_write, this, msg));
	}

	//creates a message from a string and sends it to write (written by Edmund Lewry with code modified from Kolhoff's example)
	void sendMessage(string message)
	{
		GameMessage msg;
		msg.body_length(strlen(message.c_str()));
		memcpy(msg.body(), message.c_str(), msg.body_length());
		msg.encode_header();
		write(msg);
	}

	//adds a close function call to the io_service's workload
	void close()
	{
		io_service_.post(boost::bind(&GameClient::do_close, this));
	}

	//updates the current element reference (written by Edmund Lewry)
	void updateElement(TravelElement* curr)
	{
		currElement = curr;
	}

private:
	//connect handler function, called when async_connect returns
	void handle_connect(const boost::system::error_code& error,
	tcp::resolver::iterator endpoint_iterator)
	{
		//if there was no error, then a connection has been made so start reading
		if (!error)
		{
			beginReading();
		}
		//if there was an error and the iterator is not empty
		else if (endpoint_iterator != tcp::resolver::iterator())
		{
			//close the socket and attempt a new connection with the next
			//resolver in the iterator
			socket_.close();
			tcp::endpoint endpoint = *endpoint_iterator;
			socket_.async_connect(endpoint,
			boost::bind(&GameClient::handle_connect, this,
            boost::asio::placeholders::error, ++endpoint_iterator));
		}
	}

	//called when async_read returns (first time in the cycle)
	void handle_read_header(const boost::system::error_code& error)
	{
		//if there is no error from the read
		if (!error)
		{
			//if the header can be decoded to something expected
			if(read_msg_.decode_header())
			{
				//read from socket, to read_msg_ body, body_length number of chars
				//call handle_read_body when return
				boost::asio::async_read(socket_,
				boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
				boost::bind(&GameClient::handle_read_body, this,
				boost::asio::placeholders::error));
			}
		}
		else
		{
			//close the connection
			do_close();
		}
	}

	//called when async_read returns (second in the cycle) (modified by Edmund Lewry)
	void handle_read_body(const boost::system::error_code& error)
	{
		if (!error)
		{
			//create a string from the message and parse the string
			std::string msg(read_msg_.body(), read_msg_.body_length());
			parseMsg(msg);

			//begin the reading cycle again
			boost::asio::async_read(socket_,
				boost::asio::buffer(read_msg_.data(), GameMessage::header_length),
				boost::bind(&GameClient::handle_read_header, this,
				boost::asio::placeholders::error));
		}
		else
		{
			//close connection
			cout << "ERROR BODY READ: " << error.message() << endl;
			do_close();
		}
	}

	//write a message to the socket
	void do_write(GameMessage msg)
	{
		bool write_in_progress = !write_msgs_.empty();
		//add message to queue
		write_msgs_.push_back(msg);
		
		if (!write_in_progress)
		{
			//write to socket, the data from the front message in the queue
			//call handle_write on return
			boost::asio::async_write(socket_,
				boost::asio::buffer(write_msgs_.front().data(),
				write_msgs_.front().length()),
				boost::bind(&GameClient::handle_write, this,
					boost::asio::placeholders::error));
		}
	}

	//handler for aysnc_write
	void handle_write(const boost::system::error_code& error)
	{
		if (!error)
		{
			//if no error, remove front message and start cycle again
			write_msgs_.pop_front();
			if (!write_msgs_.empty())
			{
				boost::asio::async_write(socket_,
					boost::asio::buffer(write_msgs_.front().data(),
					write_msgs_.front().length()),
					boost::bind(&GameClient::handle_write, this,
					boost::asio::placeholders::error));
			}
		}
		else
		{
			//close connection
			cout << "ERROR WRITING: " << error.message() << endl;
			do_close();
		}
	}

	//closes connection
	void do_close()
	{
		socket_.close();
	}

	//parses the string received from the server (written by Edmund Lewry)
	void parseMsg(string msg)
	{
		//if the first char is ! then it is not a serialized object
		if(msg[0]=='!')
		{
			//if the second char is & then it is a command
			if(msg[1]=='&')
			{
				//delimit the command string
				vector<string> final;
				final.push_back("");
				int currWord=0;
				for(size_t i=0; i<msg.length(); i++)
				{
					if(msg[i]==':')
					{
						currWord++;
						final.push_back("");
					}
					else
					{
						if(msg[i]!='&' && msg[i]!='!')
						{
							final[currWord].push_back(msg[i]);
						}
					}
				}

				//if the command is itemRem, remove the corresponding item
				if(final[0].compare("itemRem")==0)
				{
					for(size_t i=0; i<graph.getNodes().size(); i++)
					{
						//find the location
						if(final[1].compare(graph.getNodes()[i]->getLocation().getName())==0)
						{
							GameElement* node = graph.getNodes()[i];
							//remove item from location and element
							for(size_t j=0; j<node->getKItems().size(); j++)
							{
								if(final[2].compare(node->getKItems()[j].getName())==0)
								{
									currElement->removeKItem(j);
									node->removeKItem(j);
									node->getLocation().removeKItem(final[2]);
								}
							}
						}				
					}
				}
			}
			//if the second char was not &, then it is just text to be written
			else
			{
				string toWrite(msg,1,msg.size());
				cout << toWrite << endl;
			}
		}
		//if the first char was not ! then it is a serialized graph being read
		else
		{
			try
			{
				//create a boost archive using the message and load to the graph reference
				std::istringstream archive_stream(msg);
				boost::archive::text_iarchive archive(archive_stream);
				archive >> graph;
			}
			catch(std::exception& err)
			{
				cout << err.what() << endl;
			}
		}
	}

private:
  boost::asio::io_service& io_service_;
  tcp::socket socket_;
  GameMessage read_msg_;
  GameMessage_queue write_msgs_;

  ElementGraph& graph;
  TravelElement* currElement;
  bool connected;
};

//shared pointer type
typedef boost::shared_ptr<GameClient> game_client_ptr;

#endif