/*	StoryAgent.h
/
/	Header file for the StoryAgent class that constructs the gameplay element map.
/
*/

#include <vector>
#include "ElementGraph.h"
#include "TravelElement.h"
#include "GameObject.h"
#include "Enemy.h"

class StoryAgent
{
	public:
		StoryAgent();
		ElementGraph createStoryGraph(vector<GameObject*> programObjects);
		Location makeLocation(int i);
		Link makeLink(int i);
		KeyItem makeKItem(int i);
		Enemy makeEnemy(int i);
		bool checkForLink(Location loc, string name);
	private:
		vector<GameObject*> availableObjects;
		vector<KeyItem> allKItems;
		vector<Enemy> allEnemyTypes;
};