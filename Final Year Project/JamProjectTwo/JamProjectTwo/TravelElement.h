/*	TravelElement.h
/
/	Edmund Lewry
/
/	The header file for the TravelElement class, a child of the GameElement class.
*/

#ifndef TRAVELELEMENT_H
#define TRAVELELEMENT_H

#include "GameElement.h"
#include "Graph.h"
#include "Postbox.h"

class TravelElement : public GameElement
{
	public:
		TravelElement();
		GameElement* play(PlayerCharacter& player, Graph& gameGraph);
		GameElement* onlinePlay(postbox_ptr connect,PlayerCharacter& player, Graph& gameGraph);
		int parseInput(string input,PlayerCharacter& player, Graph& gameGraph);
		int parseInput(postbox_ptr connect, string input,PlayerCharacter& player, Graph& gameGraph);
		int parseBattleInput(string input, PlayerCharacter& player);

	private:
		int parseCommand(int command, string target,PlayerCharacter& player, Graph& gameGraph);
		int parseCommand(postbox_ptr connect, int command, string target,PlayerCharacter& player, Graph& gameGraph);
		int parseBattleCommand(int command,string target,string identifier,PlayerCharacter& player);
		void describeLocation(PlayerCharacter& player, Graph& gameGraph);
		int battle(PlayerCharacter& player);

		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & boost::serialization::base_object<GameElement>(*this);
			/*ar & kItems;
			ar & location;
			ar & type;*/
		}
		//ElementGraph& gameGraph;
};

#endif