/*	Graph.h
//
//	Edmund Lewry
//
//	Parent class of ElementGraph to avoid cyclic dependency
//
*/

#pragma once

#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include "GameElement.h"
#include "Link.h"
using namespace std;

class Graph
{
public:
	virtual ~Graph() {}
	virtual GameElement* retrieveNodeFromLink(string nodeName)=0;
	virtual vector<Link> getEdges()=0;
	virtual vector<GameElement*> getNodes()=0;
};

#endif