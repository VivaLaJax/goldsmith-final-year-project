/*	ElementGraph.h
/
/	Edmund Lewry
/
/	This is the header file for the ElementGraph class.
*/

#pragma once

#ifndef ELEMENTGRAPH_H
#define ELEMENTGRAPH_H

#include <vector>
#include <boost/serialization/vector.hpp>
#include "Graph.h"
#include "GameElement.h"
#include "TravelElement.h"
#include "Link.h"
#include "Enemy.h"

using namespace std;

class ElementGraph : public Graph
{
	public:
		ElementGraph();
		void addNode(GameElement* newNode);
		void setNodes(vector<GameElement*> newNodes);
		void addEdge(Link newLink);
		void setEdges(vector<Link> links);
		bool addKItem(KeyItem newKItem);
		void addEnemy(Enemy enemyType);
		void changeCurrentElement(GameElement* find);
		void setGraph(ElementGraph otherGraph);

		vector<Link> getEdges();
		vector<GameElement*> getNodes();
		GameElement* retrieveNodeFromLink(string nodeName);

	private:
		//serializes all data and registers the TravelElement type to prevent exceptions
		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar.register_type(static_cast<TravelElement *>(NULL));

			ar & nodes;
			ar & edges;
			ar & currentElement;
		}
		vector<GameElement*> nodes;
		vector<Link> edges;

		int currentElement;
};
#endif