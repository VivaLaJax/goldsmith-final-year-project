/* GameController.h
/
/	Edmund Lewry
/
*/

#pragma once

#include "ElementGraph.h"
#include "PlayerCharacter.h"
#include "GameClient.h"
#include "TravelElement.h"

class GameController
{
	public:
		//constructor
		GameController(ElementGraph& graph) : storyGraph(graph)	{}

		//singleplayer starts the game
		int begin()
		{
			//create character
			PlayerCharacter& pc = PlayerCharacter();

			//set current node
			currNode = storyGraph.getNodes()[0];
	
			//while current node not game end
			while(currNode->getLocation().getDesc().compare("GAME_END")!=0)
			{
				//construct travel element from game element pointer
				if(currNode->getType()==0)
				{
					TravelElement travNode = TravelElement();
					travNode.setLocation(currNode->getLocation());
					travNode.setKItems(currNode->getKItems());
					travNode.setEnemies(currNode->getEnemies());
					//next node is result of playing through this node
					currNode = travNode.play(pc,storyGraph);
				}
			}

			return 0;
		}

		//being game in online mode
		int begin(boost::shared_ptr<GameClient> connect)
		{
			//create character
			PlayerCharacter& pc = PlayerCharacter();

			//get character name
			cout << "What is your character called?" << endl;
			string name;
			cin >> name;
			pc.setName(name);

			//send name message to server
			string nameMsg = "&name:" + pc.getName();
			connect->sendMessage(nameMsg);

			//set current node
			currNode = storyGraph.getNodes()[0];
	
			//send location message to server
			string locMsg = "&loc:" + currNode->getLocation().getName();
			connect->sendMessage(locMsg);

			//send arrival message to server
			string toSend = "1|GAME_START|" + currNode->getLocation().getName() + "|" + pc.getName();
			connect->sendMessage(toSend);

			//while current node not game end (same as offline)
			while(currNode->getLocation().getDesc().compare("GAME_END")!=0)
			{
				if(currNode->getType()==0)
				{
					TravelElement travNode = TravelElement();
					travNode.setLocation(currNode->getLocation());
					travNode.setKItems(currNode->getKItems());
					travNode.setEnemies(currNode->getEnemies());
					connect->updateElement(&travNode);
					currNode = travNode.onlinePlay(connect->getSharedPtr(),pc,storyGraph);
				}
			}

			return 0;
		}

	private:
		ElementGraph& storyGraph;
		GameElement* currNode;
};