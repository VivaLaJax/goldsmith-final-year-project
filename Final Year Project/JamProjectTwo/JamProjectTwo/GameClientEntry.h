/*	GameClientEntry.h
/
/	Written by Edmund Lewry
*/

#pragma once

#include <string>
#include <vector>

#include "ElementGraph.h"
#include "GameObject.h"
#include "GameClient.h"
#include <boost/asio.hpp>

class GameClientEntry
{
	public:
		void start();
		void handle_read(const boost::system::error_code& e);
	private:
		int getGameMode();
		string getHostName();
		string getPort();
		
		vector<GameObject*> programObjects;
		ElementGraph storyGraph;
		boost::shared_ptr<GameClient> c;
};