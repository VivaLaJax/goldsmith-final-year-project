/*	ElementGraph.cpp
/
/	Edmund Lewry
/
/	This is an implementation of a graph data structure specifically for game elements to be used
/	in the Game Controller.
*/

#include "ElementGraph.h"

//constructor
ElementGraph::ElementGraph() {}

//adds a node to the graph
void ElementGraph::addNode(GameElement* newNode)
{
	nodes.push_back(newNode);
}
//sets nodes
void ElementGraph::setNodes(vector<GameElement*> newNodes)
{
	nodes = newNodes;
}


//adds a link to the graph
void ElementGraph::addEdge(Link newLink)
{
	edges.push_back(newLink);
}
//sets edges
void ElementGraph::setEdges(vector<Link> links)
{
	edges = links;
}

//add a key item to the graph if there is a location that contains it
bool ElementGraph::addKItem(KeyItem newKItem)
{
	for(size_t i=0; i<nodes.size(); i++)
	{
		vector<string> locKItems = nodes[i]->getLocation().getKeyItems();
		for(size_t j=0; j<locKItems.size(); j++)
		{
			if(newKItem.getName().compare(locKItems[j])==0)
			{
				nodes[i]->addKItem(newKItem);
				return true;
			}
		}
	}

	return false;
}

//adds an enemy to a node for each reference of that enemy type in the location of that node
void ElementGraph::addEnemy(Enemy enemyType)
{
	for(size_t i=0; i<nodes.size(); i++)
	{
		int identifier = 1;
		vector<string> locEnemies = nodes[i]->getLocation().getEnemyList();
		for(size_t j=0; j<locEnemies.size(); j++)
		{
			if(enemyType.getName().compare(locEnemies[j])==0)
			{
				nodes[i]->addEnemy(enemyType, identifier);
				identifier++;
			}
		}
	}
}

//returns all the nodes
vector<GameElement*> ElementGraph::getNodes()
{
	return nodes;
}
//returns all the links
vector<Link> ElementGraph::getEdges()
{
	return edges;
}

//returns an element based on a location name
GameElement* ElementGraph::retrieveNodeFromLink(string nodeName)
{
	GameElement* retPtr;

	for(size_t i=0; i<nodes.size(); i++)
	{
		if(nodeName.compare(nodes[i]->getLocation().getName())==0)
		{
			retPtr = nodes[i];
		}
	}

	return retPtr;
}