/*	PlayerCharacter.h
/
/	Edmund Lewry
/	
/	The header file for the PlayerCharacter class.
*/

#pragma once

#ifndef PLAYER_H
#define PLAYER_H

#include "KeyItem.h"

class PlayerCharacter
{
	public:	
		PlayerCharacter();
		bool checkInventory(vector<string> toCheck);
		void addKItem(KeyItem newKItem);
		void setName(string set);
		void calculateHP();
		void loseHP(int toLose);
		void setLastCommand(int set);
		void setLastLocation(string location);
		string getName();
		int getStrength();
		int getSpeed();
		int getEndurance();
		int getIntelligence();
		int getCurrHP();
		int getMaxHP();
		int getLastCommand();
		string getLastLocation();
	private:
		vector<KeyItem> inventory;
		string name;
		int strength;
		int speed;
		int endurance;
		int intelligence;
		int currHP;
		int maxHP;
		int lastCommand;
		string previousLocation;
};

#endif