/*	JSLInterpreter.h
/
/	Edmund Lewry
/
/	This is the header file for the JSLInterpreter class.
*/

#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

#include "GameObject.h"
#include "Location.h"
#include "KeyItem.h"
#include "Link.h"
#include "Enemy.h"

using namespace std;

class JSLInterpreter
{
	public:
		JSLInterpreter();
		vector<GameObject*> interpret();

	private:
		vector<GameObject*> outObjs;
};