/*	GameObject.h
/
/	Edmund Lewry
/
/	The header file for the GameObject class.
*/

#pragma once

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <vector>
#include <boost/archive/text_oarchive.hpp>

using namespace std;

class GameObject
{
	protected:
		//serializes the member variable of the instance
		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & type;
		}
		int type;
	public:
		GameObject();
		int getType() { return type; }

		//for all
		virtual string getName() { return ""; }
		virtual string getDesc() { return ""; }
		virtual vector<string> getTags();

		//for location
		virtual vector<string> getKeyItems();
		virtual vector<string> getEnemyList();

		//for link
		virtual string getToLoc() { return ""; }
		virtual string getFromLoc() { return ""; }
		virtual string getStatDesc() { return ""; }
		virtual string getDynDesc() { return ""; }
		virtual vector<string> getRequiredItem();

		//for enemy
		virtual vector<int> getAllStats();
		virtual int getStrength() { return 0; }
		virtual int getSpeed() { return 0; }
		virtual int getEndurance() { return 0; }
		virtual int getIntelligence() { return 0; }
		virtual int getMaxHP() { return 0; }
		virtual int getCurrHP() { return 0; }
		virtual int getMaxSP() { return 0; }
		virtual int getCurrSP() { return 0; }
		virtual int getXPVal() { return 0; }
};

#endif