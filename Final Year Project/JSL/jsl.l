%{
#include <iostream>
#include <string.h>
#include "jsl.tab.h"
using namespace std;
#define YY_DECL extern "C" int yylex()
%}

%option noyywrap

STRING       	\".*\"
STRINGARR	"{"({STRING}(","{STRING})*)?"}"
INTEGER		[0-9]+
%%
[ \t] ;
CREATE_GAME    { return GAME; }
define         { return DEFINE; }	
as	       { return AS; }
LOCATION       { return LOCATION; }
LocName	       { return LOCNAME; }
LocDesc	       { return LOCDESC; }
LocKItems      { return LOCKITEMS; }
LocTags        { return LOCTAGS; }
LocEnemies     { return LOCENEMIES; }
KEYITEM        { return KEYITEM; }
KItemName      { return KITEMNAME; }
KItemDesc      { return KITEMDESC; }
KItemTags      { return KITEMTAGS; }
LINK	       { return LINK; }
LinkToLoc      { return LINKTOLOC; }
LinkFromLoc    { return LINKFROMLOC; }
LinkStatDesc   { return LINKSTATDESC; }
LinkDynDesc    { return LINKDYNDESC; }
LinkItemReq    { return LINKITEMREQ; }
ENEMY 	       { return ENEMY; }
EnemyName      { return ENEMYNAME; }
EnemyDesc      { return ENEMYDESC; }
EnemyTags      { return ENEMYTAGS; }
EnemyStr       { return ENEMYSTR; }
EnemySpd       { return ENEMYSPD; }
EnemyEnd       { return ENEMYEND; }
EnemyInt       { return ENEMYINT; }
EnemyExp       { return ENEMYXP; }
\(	       { return OPEN_BLOCK; }
\)             { return CLOSE_BLOCK; }
"="	       { return EQ; }
;	       { return END_LINE; }
{STRING}       {
	// we have to copy because we can't rely on yytext not changing underneath us:
	yylval.sval = strdup(yytext);
	return STRING;
}
{STRINGARR}    {
	yylval.sval = strdup(yytext);
	return STRINGARR;
}
{INTEGER}	       {
	yylval.ival = atoi(yytext); 
	return INTEGER;
}
.              ;
%%