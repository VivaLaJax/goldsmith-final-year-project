
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     GAME = 258,
     DEFINE = 259,
     AS = 260,
     LOCATION = 261,
     KEYITEM = 262,
     LINK = 263,
     ENEMY = 264,
     LOCNAME = 265,
     LOCDESC = 266,
     LOCKITEMS = 267,
     LOCTAGS = 268,
     LOCENEMIES = 269,
     KITEMNAME = 270,
     KITEMDESC = 271,
     KITEMTAGS = 272,
     LINKTOLOC = 273,
     LINKFROMLOC = 274,
     LINKSTATDESC = 275,
     LINKDYNDESC = 276,
     LINKITEMREQ = 277,
     ENEMYNAME = 278,
     ENEMYDESC = 279,
     ENEMYTAGS = 280,
     ENEMYSTR = 281,
     ENEMYSPD = 282,
     ENEMYEND = 283,
     ENEMYINT = 284,
     ENEMYXP = 285,
     OPEN_BLOCK = 286,
     CLOSE_BLOCK = 287,
     END_LINE = 288,
     EQ = 289,
     INTEGER = 290,
     STRING = 291,
     STRINGARR = 292
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 57 "jsl.y"

	int ival;
	float fval;
	char* sval;



/* Line 1676 of yacc.c  */
#line 97 "jsl.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


