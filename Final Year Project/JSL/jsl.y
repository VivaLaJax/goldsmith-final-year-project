%{
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include "GameObject.h"
#include "Location.h"
#include "KeyItem.h"
#include "Link.h"
#include "Enemy.h"
#include <boost/archive/text_oarchive.hpp>
using namespace std;

#include "jsl.tab.h"  // to get the token types that we return

// stuff from flex that bison needs to know about:
extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;



Location tempLoc;
vector<Location> vecLoc;
int locCount = 0;
Link tempLink;
vector<Link> vecLink;
int linkCount = 0;
KeyItem tempItem;
vector<KeyItem> vecItem;
int kitCount = 0;
Enemy tempEnemy;
vector<Enemy> vecEnemy;
int enCount = 0;

ofstream locwriter("locationobjects.txt");
ofstream kitwriter("kitemobjects.txt");
ofstream linkwriter("linkobjects.txt");
ofstream enemywriter("enemyobjects.txt");
boost::archive::text_oarchive loca(locwriter);
boost::archive::text_oarchive kita(kitwriter);
boost::archive::text_oarchive lina(linkwriter);
boost::archive::text_oarchive enea(enemywriter);

void yyerror(const char *s);
vector<string> seperate(char *s);
string cleanString(char *s);
void closeAllWriters();
%}

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  But tokens could be of any
// arbitrary data type!  So we deal with that in Bison by defining a C union
// holding each of the types of tokens that Flex could return, and have Bison
// use that union instead of "int" for the definition of "yystype":
%union {
	int ival;
	float fval;
	char* sval;
}

// define the constant-string tokens:
%token GAME DEFINE AS LOCATION KEYITEM LINK ENEMY
%token LOCNAME LOCDESC LOCKITEMS LOCTAGS LOCENEMIES
%token KITEMNAME KITEMDESC KITEMTAGS
%token LINKTOLOC LINKFROMLOC LINKSTATDESC LINKDYNDESC LINKITEMREQ
%token ENEMYNAME ENEMYDESC ENEMYTAGS ENEMYSTR ENEMYSPD ENEMYEND ENEMYINT ENEMYXP
%token OPEN_BLOCK CLOSE_BLOCK END_LINE EQ

// define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <ival> INTEGER
//%token <fval> FLOAT
%token <sval> STRING
%token <sval> STRINGARR

%%

// the first rule defined is the highest-level rule, which in our
// case is just the concept of a whole "snazzle file":
game:
	header definition_section footer { closeAllWriters(); cout << "read game file!" << endl; }
	;
header:
	GAME OPEN_BLOCK { cout << "reading a game file" << endl; }
	;
definition_section:
	definition_blocks
	;
definition_blocks:
	definition_blocks definition_block
	| definition_block
	;
definition_block:
	location_block
	| keyitem_block
	| link_block
	| enemy_block
	;
location_block:
	DEFINE LOCATION AS OPEN_BLOCK location_vars CLOSE_BLOCK { cout << "new location definition " << endl; 
locCount++;}
	;
keyitem_block:
	DEFINE KEYITEM AS OPEN_BLOCK keyitem_vars CLOSE_BLOCK { cout << "new key item definition " << endl; 
kitCount++;}
	;
link_block:
	DEFINE LINK AS OPEN_BLOCK link_vars CLOSE_BLOCK { cout << "new link definition " << endl; 
linkCount++;}
	;
enemy_block:
	DEFINE ENEMY AS OPEN_BLOCK enemy_vars CLOSE_BLOCK { cout << "new enemy definition " << endl; 
enCount++;}
	;
location_vars:
	locname_line locdesc_line lockitems_line loctags_lines locenemies_line
	;
locname_line:
	LOCNAME EQ STRING END_LINE { tempLoc = Location();
vecLoc.push_back(tempLoc);
vecLoc[locCount].setName(cleanString($3));
cout << "LocName = " << vecLoc[locCount].getName() << endl;}
	;
locdesc_line:
	LOCDESC EQ STRING END_LINE { vecLoc[locCount].setDesc(cleanString($3));
cout << "LocDesc = " << vecLoc[locCount].getDesc() << endl;}
	;
lockitems_line:
	LOCKITEMS EQ STRINGARR END_LINE { vecLoc[locCount].setKeyItems(seperate($3));
cout<< "LocKeyItems are:" << endl;
for(int i=0; i<vecLoc[locCount].getKeyItems().size(); i++){
cout << vecLoc[locCount].getKeyItems()[i] << endl;}}
	;
loctags_lines:
	LOCTAGS EQ STRINGARR END_LINE { vecLoc[locCount].setTags(seperate($3));
cout << "Location: There are tags" << endl;}
	;
locenemies_line:
	LOCENEMIES EQ STRINGARR END_LINE { vecLoc[locCount].setEnemyList(seperate($3));
cout << "Location: There are enemies" << endl;}
	;
keyitem_vars:
	kitenname_line kitemdesc_line kitemtags_line
	;
kitenname_line:
	KITEMNAME EQ STRING END_LINE { tempItem = KeyItem();
vecItem.push_back(tempItem);
vecItem[kitCount].setName(cleanString($3));
cout << "KItemName = " << vecItem[kitCount].getName() << endl; }
	;
kitemdesc_line:
	KITEMDESC EQ STRING END_LINE { vecItem[kitCount].setDesc(cleanString($3));
cout << "KItemDesc = " << vecItem[kitCount].getDesc() << endl; }
	;
kitemtags_line:
	KITEMTAGS EQ STRINGARR END_LINE { vecItem[kitCount].setTags(seperate($3));
cout << "KItem: There are tags" << endl;}
	;
link_vars:
	linktoloc_line linkfromloc_line linkstatdesc_line linkdyndesc_line linkobjreq_line
	;
linktoloc_line:
	LINKTOLOC EQ STRING END_LINE { tempLink = Link();
vecLink.push_back(tempLink);
vecLink[linkCount].setToLoc(cleanString($3));
cout << "LinkToLoc = " << vecLink[linkCount].getToLoc() << endl; }
	;
linkfromloc_line:
	LINKFROMLOC EQ STRING END_LINE { vecLink[linkCount].setFromLoc(cleanString($3));
cout << "LinkFromLoc = " << vecLink[linkCount].getFromLoc() << endl; }
	;
linkstatdesc_line:
	LINKSTATDESC EQ STRING END_LINE { vecLink[linkCount].setStatDesc(cleanString($3));
cout << "LinkStatDesc = " << vecLink[linkCount].getStatDesc() << endl;}
	;
linkdyndesc_line:
	LINKDYNDESC EQ STRING END_LINE { vecLink[linkCount].setDynDesc(cleanString($3));
cout << "LinkDynDesc = " << vecLink[linkCount].getDynDesc() << endl; }
	;
linkobjreq_line:
	LINKITEMREQ EQ STRINGARR END_LINE { vecLink[linkCount].setRequiredItem(seperate($3));
cout << "Link: There are items" << endl; }
	;
enemy_vars:
	enemyName_line enemyDesc_line enemyTags_line enemyStr_line enemySpd_line enemyEnd_line enemyInt_line enemyXp_line
	;
enemyName_line:
	ENEMYNAME EQ STRING END_LINE { tempEnemy = Enemy();
vecEnemy.push_back(tempEnemy);
vecEnemy[enCount].setName(cleanString($3)); 
cout << "EnemyName = " << vecEnemy[enCount].getName() << endl; }
	;
enemyDesc_line:
	ENEMYDESC EQ STRING END_LINE { vecEnemy[enCount].setDesc(cleanString($3));
cout << "EnemyDesc = " << vecEnemy[enCount].getDesc() << endl; }
	;
enemyTags_line:
	ENEMYTAGS EQ STRINGARR END_LINE { vecEnemy[enCount].setTags(seperate($3));
cout << "Enemy: There are tags" << endl;}
	;
enemyStr_line:
	ENEMYSTR EQ INTEGER END_LINE { vecEnemy[enCount].setStrength($3);
cout << "EnemyStr = " << vecEnemy[enCount].getStrength() << endl; }
	;
enemySpd_line:
	ENEMYSPD EQ INTEGER END_LINE { vecEnemy[enCount].setSpeed($3);
cout << "EnemySpd = " << vecEnemy[enCount].getSpeed() << endl; }
	;
enemyEnd_line:
	ENEMYEND EQ INTEGER END_LINE { vecEnemy[enCount].setEndurance($3);
cout << "EnemyEnd = " << vecEnemy[enCount].getEndurance() << endl; }
	;
enemyInt_line:
	ENEMYINT EQ INTEGER END_LINE { vecEnemy[enCount].setIntelligence($3);
cout << "EnemyInt = " << vecEnemy[enCount].getIntelligence() << endl; }
	;
enemyXp_line:
	ENEMYXP EQ INTEGER END_LINE { vecEnemy[enCount].setXP($3);
cout << "EnemyXP = " << vecEnemy[enCount].getXPVal() << endl; }
	;
footer:
	CLOSE_BLOCK END_LINE
	;

%%

main() {
	// open a file handle to a particular file:
	FILE *myfile = fopen("inputSimple.jsl", "r");
	// make sure it's valid:
	if (!myfile) {
		cout << "I can't open a.jsl.file!" << endl;
		return -1;
	}
	// set flex to read from it instead of defaulting to STDIN:
	yyin = myfile;

	// parse through the input until there is no more:
	do {
		yyparse();
	} while (!feof(yyin));
	
}

void yyerror(const char *s) {
	cout << "EEK, parse error!  Message: " << s << endl;
	// might as well halt now:
	exit(-1);
}

vector<string> seperate(char* s)
{
	vector<string> retVal;
	string currWord = "";
	string fullString = string(s);
	for(int i=0; i<fullString.size(); i++)
	{
		if(fullString[i] == ',' || fullString[i] == '}')
		{
			retVal.push_back(currWord);
			currWord = "";
		}
		else
		{
			if(fullString[i]!='{' && fullString[i]!='"')
			{
				currWord.push_back(fullString[i]);
			}
		}
	}

	return retVal;
}

string cleanString(char* s)
{
	string currWord = "";
	string fullString = string(s);
	for(int i=0; i<fullString.size(); i++)
	{
		if(fullString[i]!='"')
		{
			currWord.push_back(fullString[i]);
		}
	}

	return currWord;
}

void closeAllWriters()
{
	loca << vecLoc;
	kita << vecItem;
	lina << vecLink;
	enea << vecEnemy;

	locwriter.close();
	kitwriter.close();
	linkwriter.close();
	enemywriter.close();
}