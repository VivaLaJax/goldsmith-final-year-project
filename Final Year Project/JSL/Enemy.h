/* Enemy.h
//
// The header file for the Enemy class, a child of the GameObject class.
//
*/

#ifndef ENEMY_H
#define ENEMY_H

#include <string>
#include <vector>
#include "GameObject.h"
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>

using namespace std;

class Enemy : public GameObject
{
public:
	Enemy();
	
	//getters
	string getName() { return enName; }
	string getDesc() { return enDesc; }
	vector<string> getTags() { return tags; }
	vector<int> getAllStats();
	int getStrength() { return strength; }
	int getSpeed() { return speed; }
	int getEndurance() { return endurance; }
	int getIntelligence() { return intelligence; } 
	int getMaxHP() { return maxHealthPoints; }
	int getCurrHP() { return healthPoints; }
	int getMaxSP() { return maxSpecialPoints; }
	int getCurrSP() { return specialPoints; }
	int getXPVal() { return expPoints; }
	
	//setters
	void setName(string name) { enName = name; }
	void setDesc(string desc) { enDesc = desc; }
	void setTags(vector<string> tagList) { tags = tagList; }
	void setStrength(int val) { strength = val; }
	void setSpeed(int val) { speed = val; }
	void setEndurance(int val) { endurance = val; }
	void setIntelligence(int val) { intelligence = val; }
	void setCurrHP(int val) { healthPoints = val; }
	void setCurrSP(int val) { specialPoints = val; }
	void calculateMaxHP();
	void calculateMaxSP();
	void setXP(int val) { expPoints = val; }

	bool findTag(string tag);

private:
	//serializes the member variables of the instance, and calls serialization of parent
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<GameObject>(*this);
		ar & strength;
		ar & speed;
		ar & endurance;
		ar & intelligence;
		ar & maxHealthPoints;
		ar & healthPoints;
		ar & maxSpecialPoints;
		ar & specialPoints;
		ar & expPoints;
		ar & enName;
		ar & enDesc;
		ar & tags;
		ar & type;
	}

	int strength;
	int speed;
	int endurance;
	int intelligence;
	int maxHealthPoints;
	int healthPoints;
	int maxSpecialPoints;
	int specialPoints;
	int expPoints;
	string enName;
	string enDesc;
	vector<string> tags;
	int type;
};

#endif